import { Contact } from '../layout/footer'
import { Navigation } from '../layout/navigation'
import {
  Container, Col, Row, Jumbotron, Button, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, FormGroup, Input
} from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import React, { useEffect, useState } from 'react';
import { IRootState } from '../redux/store';
import { push } from 'connected-react-router';
import { useForm } from 'react-hook-form';
import { exerciseThunk } from '../redux/exercise/thunk';
import { autoLoginThunk } from '../redux/auth/thunk';
import './ExercisePage.css'

const {REACT_APP_API_SERVER} = process.env


export default function ExercisePage() {
  const dispatch = useDispatch();
  //testing testing testing testing testing testing testing testing testing testing testing testing testing testing 
  const selectedVideoId = useSelector((state:IRootState)=> state.video.selectedVideo)
  const [Video, setVideo] = useState<any>(null)
  const utubeVideoId = `${Video?Video.youtube_video_id:''}`
  const [Exercise, setExercise] = useState<any>(null)
  const info = useSelector((state: IRootState) => state.auth.user)

  
  useEffect(() => {
    if (!info)
    dispatch(autoLoginThunk())
  }, [info])



  useEffect(() => {
    fetch(`${REACT_APP_API_SERVER}/subtitle?vid=`+selectedVideoId)
      .then(response => response.json())
      .then(data => setVideo(data[0])); //data[0] coz it is an array
  }, []);

  useEffect(() => {
    fetch(`${REACT_APP_API_SERVER}/exercise?vid=`+selectedVideoId)
      .then(response => response.json())
      .then(data => setExercise(data)); 
  }, []);
  console.log("fetch exercise", Exercise)

  type FormState = {
    // be careful be careful be careful be careful be careful be careful be careful be careful 
    exerciseId: number
    userId?: number,
    answer1: string,
    answer2: string,
    answer3: string,
    answer4: string,
    answer5: string,
}

  const { register, handleSubmit } = useForm<FormState>({
    defaultValues: {
      // be careful be careful be careful be careful be careful be careful be careful be careful 
      exerciseId: parseInt(selectedVideoId),
      userId: info?.id,
      answer1: '',
      answer2: '',
      answer3: '',
      answer4: '',
      answer5: '',
    }
})


  function onSubmit(state: FormState) {
    console.log('onSubmit from exercise:', state);
    dispatch(exerciseThunk(state))


}

console.log("user icon: ", info?.user_icon);
console.log("user email: ", info?.email);

  //testing
  return (
    <div>
      <Navigation />
      {/* <Container style={{ display:"flex", flex:"wrap",width: "950px", marginTop:"5rem" }}> */}
      {/* 210629 orginal */}
      {/* <div className="containerExercise" > */}

      <div className="containerExercise" style={{ minHeight:"80vh"}}>
      <Container style={{ marginTop:"5rem" }}>
        {/* <UserAndVideo /> */}
        <Row style={{display: "flex", justifyContent: "center"}}>
          {/* <Row> */}

        <Col  md={3} style={{display: "flex", justifyContent: "center"}}>
          <Card className="exerciseUserCard" > 
            <div >
            {info?.user_icon ? <CardImg className="exerciseProfilePic" style={{ borderRadius:'50%',width: "200px", height: "200px" }} src={info.user_icon} alt="Card image cap" ></CardImg> 
            : <CardImg className="exerciseProfilePic" style={{ width: "200px", height: "200px" }} src="/img/dummy_user.png" alt="Card image cap" ></CardImg>}
            <CardBody >
              <CardTitle tag="h2" style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                {info?.username}
                
                </CardTitle>
                {/* <h2>User:</h2>
                <h3>{info?.username}</h3> */}
              {/* <CardText></CardText> */}
            </CardBody>
            </div>
          </Card>
        </Col>
        {/* <Col md={{ size: 4, offset: 4 }}>{`md={{ span: 4, offset: 4 }}`}</Col> */}

        <Col md={{size:5,offset:1,}}>
        {
          utubeVideoId?
          <iframe className="iframeExercise" src={"https://www.youtube.com/embed/" + utubeVideoId + "?enablejsapi=1&controls=0"}
          // <iframe className="iframeExercise" width="560" height="315" src={"https://www.youtube.com/embed/" + utubeVideoId + "?enablejsapi=1&controls=0"}
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          ></iframe>
          :''
        }
        </Col>

      </Row >

        <div>
          {/* <Row style={{ display: "flex", justifyContent: "center" }}>
        <Col md={12}> */}
        <div style={{ display: "flex", justifyContent: "center" ,}}>
          <Jumbotron style={{ width: "900px", height: "350px", margin: "20px", marginBottom: "40px", overflowX: "scroll" }}>
            <Container fluid>
              <p className="lead-2">
               {Exercise && Exercise.length>0 ?Exercise[0].content:'Loading...'}
            </p>
            </Container>
          </Jumbotron>
        </div>
        {/* </Col>
        </Row> */}
     
     <form onSubmit={handleSubmit(onSubmit)}>
            {/* <h3>-----------------testing----------------- Your Answer -----------------testing----------------- </h3> */}
            <Row style={{display:"flex", alignItems:"baseline",justifyContent:"space-evenly", marginBottom:"30px"}}>
            {/* be careful be careful be careful be careful be careful be careful be careful be careful  */}
            <div className="form-group">
                <input type="hidden"  className="form-control" {...register('userId')} style={{ textAlign: "center", fontSize:"1.3rem" }} />
              </div>
              <div className="form-group">
                <input type="hidden"className="form-control" {...register('exerciseId')} style={{ textAlign: "center", fontSize:"1.3rem" }}  />
              </div>
              <div className="form-group">
                <input type="text" placeholder="Q1" className="form-control" {...register('answer1')} style={{ textAlign: "center", fontSize:"1.3rem" }}  />
              </div>
              <div className="form-group">
                {/* <label>Q2</label> */}
                <input type="text" placeholder="Q2" className="form-control" {...register('answer2')} style={{ textAlign: "center", fontSize:"1.3rem" }} />
              </div>
              <div className="form-group">
                {/* <label>Q3</label> */}
                <input type="text" placeholder="Q3" className="form-control" {...register('answer3')} style={{ textAlign: "center", fontSize:"1.3rem" }} />
              </div>
              <div className="form-group">
                {/* <label>Q4</label> */}
                <input type="text" placeholder="Q4" className="form-control" {...register('answer4')} style={{ textAlign: "center", fontSize:"1.3rem" }} />
              </div>

              <div className="form-group">
                {/* <label>Q5</label> */}
                <input type="text" placeholder="Q5" className="form-control" {...register('answer5')} style={{ textAlign: "center", fontSize:"1.3rem" }} />

              </div>



              <Button style={{fontSize:"1.3rem"}} type="submit" className="exercise-submit-button">Check Answer</Button>
            </Row>
          </form>


    </div>
      </Container>
      </div>
      
      <Contact />
    </div>
  )
}

