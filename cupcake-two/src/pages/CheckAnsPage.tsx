import { Contact } from '../layout/footer'
import { Navigation } from '../layout/navigation'
import {
  Container, Col, Row, Jumbotron, Button, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, FormGroup, Input, CardHeader
} from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import React, { useEffect, useState } from 'react';
import { IRootState } from '../redux/store';
import { push } from 'connected-react-router';
import { useForm } from 'react-hook-form';
import { exerciseThunk } from '../redux/exercise/thunk';
import AnsComparison from '../components/ansComparison';
import { useParams } from 'react-router';
import './CheckAnsPage.css'

const {REACT_APP_API_SERVER} = process.env


export default function ExercisePage() {


  const dispatch = useDispatch();
  //testing testing testing testing testing testing testing testing testing testing testing testing testing testing 
  const selectedVideoId = useSelector((state:IRootState)=> state.video.selectedVideo)
  const [Video, setVideo] = useState<any>(null)
  const utubeVideoId = `${Video?Video.youtube_video_id:''}`
  const [Exercise, setExercise] = useState<any>(null)

  const info = useSelector((state: IRootState) => state.auth.user)


  useEffect(() => {
    fetch(`${REACT_APP_API_SERVER}/subtitle?vid=`+selectedVideoId)
      .then(response => response.json())
      .then(data => setVideo(data[0])); //data[0] coz it is an array
  }, []);
  useEffect(() => {
    fetch(`${REACT_APP_API_SERVER}/exercise?vid=`+selectedVideoId)
      .then(response => response.json())
      .then(data => setExercise(data)); 
  }, []);

  const [UserAnswer, setUserAnswer] = useState<any>(null)
  useEffect(() => {
    // related to ex-controller row 49
    //edgar
    // fetch('http://localhost:8100/get-user-answer?userId='+info?.id+'&userExerciseId='+selectedVideoId)
    fetch(`${REACT_APP_API_SERVER}/get-user-answer?userId=`+info?.id+'&userExerciseId='+selectedVideoId)

      .then(response => response.json())
      .then(data => setUserAnswer(data)); 
  }, []);
  const [ModelAnswer, setModelAnswer] = useState<any>(null)
  useEffect(() => {
    //edgar
    fetch(`${REACT_APP_API_SERVER}/get-model-answer?ansId=`+selectedVideoId)
      .then(response => response.json())
      .then(data => setModelAnswer(data)); 
  }, []);
  
  console.log("fetch exercise", Exercise)

  type FormState = {
    // exerciseId: number
    // userId: number|null
    Q1: string,
    Q2: string,
    Q3: string,
    Q4: string,
    Q5: string,
    // text:String
}
  const { register, handleSubmit } = useForm<FormState>({
    defaultValues: {
      // exerciseId: 10,
      // userId: null
      Q1: '',
      Q2: '',
      Q3: '',
      Q4: '',
      Q5: '',
      // text:''
    }
})

  function onSubmit(state: FormState) {
    console.log('onSubmit from exercise:', state);
    dispatch(exerciseThunk(state))


}
  //testing
  return (
    <div>
      <Navigation />
      {/* <Container style={{ display:"flex", flex:"wrap",width: "950px", marginTop:"5rem" }}> */}
        {/* 210629 orginal */}
      {/* <div className="containerExercise" > */}
      <div className="containerExercise" style={{ minHeight:"80vh"}}>
        
      <Container style={{ marginTop:"5rem" }}>
        {/* <UserAndVideo /> */}
        <Row style={{display: "flex", justifyContent: "center"}}>
          {/* <Row> */}

        <Col  md={3} style={{display: "flex", justifyContent: "center"}}>
          <Card className='ansPageUserCard'> 
            <div >
            <CardImg className="ansPageProfilePic" style={{ width: "200px", height: "200px" ,marginLeft:"20px", marginRight:"20px" }} src="/img/dummy_user.png" alt="Card image cap" />
            {/* <div style={{ width: "200px", height: "200px" }} >src="/img/dummy_user.png</div> */}
            {/* <img style={{ width: "200px", height: "200px" , display:"flex", justifyContent:""}} src="/img/dummy_user.png"/> */}

            <CardBody>
              <CardTitle tag="h2" style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                {info?.username}</CardTitle>
              
               
                <div style={{display:"flex" }}>
                  <div style={{margin:"5px"}}>
                    <h3>Score</h3>
                    <h4 style={{ display: "flex", alignItems: "center", justifyContent: "center"}} >
                    {UserAnswer && UserAnswer.length > 0 ? UserAnswer[0].attempt : '' } 
                    </h4>
                  </div>
        
                  <div style={{margin:"5px"}}>
                    <h3>Your Best Record</h3>
                    <h4 style={{ display: "flex", alignItems: "center", justifyContent: "center"}} >
                    {UserAnswer && UserAnswer.length > 0 ? UserAnswer[0].highest_score : ''} 
                    </h4>
                  </div>
                </div>
               
                </CardBody>
            </div>
          </Card>
        </Col>
        {/* <Col md={{ size: 4, offset: 4 }}>{`md={{ span: 4, offset: 4 }}`}</Col> */}

        <Col md={{size:5,offset:1,}}>
        {
          utubeVideoId?
          <iframe className="iframeAnsPage" src={"https://www.youtube.com/embed/" + utubeVideoId + "?enablejsapi=1&controls=0"}
          // <iframe className="iframeExercise" width="560" height="315" src={"https://www.youtube.com/embed/" + utubeVideoId + "?enablejsapi=1&controls=0"}
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          ></iframe>
          :''
        }
        </Col>
        </Row >


        <Row style={{display: "flex", justifyContent: "center"}}>
                    <Col md={3} style={{display: "flex", justifyContent: "center"}}> <Card style={{
                        width: "300px", height: "175px", display: "flex", borderRadius: "15px"}} >
                        <CardHeader tag="h3" style={{ display: "flex", alignItems: "center", justifyContent: "center", 
                        backgroundColor: "aliceBlue", borderTopLeftRadius: "15px", borderTopRightRadius: "15px" }}>Your Answer</CardHeader>
                        <CardBody>
                            <CardText >
                            1. {UserAnswer && UserAnswer.length>0 ?UserAnswer[0].posted_answer1:''}<br />
                            2. {UserAnswer && UserAnswer.length>0 ?UserAnswer[0].posted_answer2:''}<br />                           
                            3. {UserAnswer && UserAnswer.length>0 ?UserAnswer[0].posted_answer3:''}<br />
                            4. {UserAnswer && UserAnswer.length>0 ?UserAnswer[0].posted_answer4:''}<br />
                            5. {UserAnswer && UserAnswer.length>0 ?UserAnswer[0].posted_answer5:''}</CardText>
                        </CardBody>
                    </Card></Col>
                    <Col  md={3} style={{display: "flex", justifyContent: "center"}}> <Card style={{
                        width: "300px", height: "175px", display: "flex", borderRadius: "15px"}} >
                        <CardHeader tag="h3" style={{ display: "flex", alignItems: "center", justifyContent: "center", 
                        backgroundColor: "lavender", borderTopLeftRadius: "15px", borderTopRightRadius: "15px" }}>Correct Answer</CardHeader>
                        <CardBody>
                            <CardText>
                            1. {ModelAnswer && ModelAnswer.length>0 ?ModelAnswer[0].model_answer1:''}<br />
                            2. {ModelAnswer && ModelAnswer.length>0 ?ModelAnswer[0].model_answer2:''}<br />                            
                            3. {ModelAnswer && ModelAnswer.length>0 ?ModelAnswer[0].model_answer3:''}<br /> 
                            4. {ModelAnswer && ModelAnswer.length>0 ?ModelAnswer[0].model_answer4:''}<br />
                            5. {ModelAnswer && ModelAnswer.length>0 ?ModelAnswer[0].model_answer5:''}</CardText>
                        </CardBody>
                    </Card></Col>
                </Row>
          {/* <AnsComparison /> */}
       
   
        <div>
        <div style={{ display: "flex", justifyContent: "center" ,}}>
          <Jumbotron style={{ width: "900px", height: "350px", margin: "20px", marginBottom: "40px", overflowX: "scroll" }}>
            <Container fluid>
              <p className="lead-2">
               {Exercise && Exercise.length>0 ?Exercise[0].content:'Loading...'}
            </p>
            </Container>
          </Jumbotron>
          
        </div>
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <Button style={{ margin: "15px", fontSize:"1.6rem" }} variant="info" onClick={() => dispatch(push('/'))}>Back to home</Button>{' '}
        </div>
    </div>
  
      </Container>
      </div>
      <Contact />
    </div>
  )
}

