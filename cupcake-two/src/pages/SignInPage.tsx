import React from "react";
import { useDispatch } from 'react-redux';
import ReactFacebookLogin, { ReactFacebookFailureResponse, ReactFacebookLoginInfo } from 'react-facebook-login';
import { loginFacebookThunk } from "../redux/fbAuth/thunk";
import { useForm } from "react-hook-form";
// import { Form, Label, Button } from "reactstrap";
import { loginThunk } from '../redux/auth/thunk';
import './Register.css'
import { ImFontSize } from "react-icons/im";

const { REACT_APP_FACEBOOK_APP_ID } = process.env

declare var FB: any;

function facebookAuthOnClick() {
    console.log('onClick');

    // FB.init({
    //     appId: '{your-app-id}',
    //     cookie: true,
    //     xfbml: true,
    //     version: '{api-version}'
    // });

    FB.AppEvents.logPageView();

}



export default function SignInPage() {
    // const [username, setUsername] = useState('')
    // const [password, setPassword] = useState('')

    const dispatch = useDispatch()

    type FormState = {
        username: string,
        password: string
    }
    const { register, handleSubmit } = useForm<FormState>({
        defaultValues: {
            username: "",
            password: ""
        }
    })

    function onSubmit(state: FormState) {
        console.log('on from login :', state);
        dispatch(loginThunk(state))

    }

    function facebookAuthCallBack(data: ReactFacebookFailureResponse | ReactFacebookLoginInfo) {
        console.log('data: ', data);
        if ('accessToken' in data) {
            dispatch(loginFacebookThunk(data.accessToken))
        }
    }

    return (
        <div className="outer">
                            <div className="inner">

                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <h3 style={{fontSize:'3rem', fontWeight:'bold'}}>Log in</h3>

                                    <div className="form-group">
                                        <label style={{fontSize:'2rem', marginTop:'6rem'}}>Username</label>
                                        <input  style={{fontSize:'2rem', marginTop:'1.8rem'}} type="text" className="form-control" {...register('username')} />
                                    </div>

                                    <div className="form-group">
                                        <label style={{fontSize:'2rem', marginTop:'2rem'}}>Password</label>
                                        <input  style={{fontSize:'2rem', marginTop:'1.8rem'}}type="password" className="form-control" {...register('password')} />
                                    </div>

                                    <div style={{marginTop:'2rem'}}className="form-group">
                                        <div className="custom-control custom-checkbox">
                                            <input type="checkbox" className="custom-control-input" id="customCheck1" />
                                            <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                                        </div>
                                    </div>

                                    <button style={{fontSize:'2rem', marginTop:'2rem'}} type="submit" className="btn btn-dark btn-lg btn-block">Sign in</button>
                                    <p className="forgot-password text-right">
                                        Forgot password?
                                    </p>

                                    <p className="forgot-password text-center" style={{ fontSize: "2rem", fontWeight:"bold"}}>
                                        OR 
                                    </p>

                                    <div className="btn btn-lg btn-block" style={{ padding: "2rem"}}>
                                        {/* {REACT_APP_FACEBOOK_APP_ID} */}
                                        <ReactFacebookLogin
                                            appId={REACT_APP_FACEBOOK_APP_ID!}

                                            autoLoad={false}
                                            fields='name,email,picture'
                                            onClick={facebookAuthOnClick}
                                            callback={facebookAuthCallBack}
                         
                                             />
                                    </div>


                                    

                                </form>

                            </div>
                        </div>



    );
}

