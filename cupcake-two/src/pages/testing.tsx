import VideoBox from '../components/videoBox'
import { Contact } from '../layout/footer'
import { Navigation } from '../layout/navigation'
import { Container, Row, Col } from 'reactstrap';

export default function VideoPage(props:any) {
  return (
    <div>


          <Row>
              {props.data
                  ? props.data.map((dataItem: any, dataIndex: any) => (
                      <div key={`${dataItem.title}-${dataIndex}`} className='col-xs-6 col-md-3'>
                          {' '}
                          <i className={dataItem.icon}></i>
                          <h3>{dataItem.title}</h3>
                         
                      </div>
                  ))
                  : 'Loading...'}

          </Row>


      <Navigation />
      <Container>
      <h1>
        BBC
      </h1>
      <Row>
      <Col><VideoBox/></Col>
      <Col><VideoBox/> </Col>
      <Col><VideoBox/></Col>
      </Row>
      <br></br>
      <Row>
      <Col><VideoBox/></Col>
      <Col><VideoBox/> </Col>
      <Col><VideoBox/></Col>
      </Row>
      </Container>
      <br></br>
      <br></br>
      <br></br>
      <Contact/>
      {/* <Contact /> */}
      
   
    </div>
  )
}

