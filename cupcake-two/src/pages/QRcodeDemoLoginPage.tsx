import React, { useState, useRef, useEffect } from 'react';
import { Container, Card, CardContent, makeStyles, Grid, TextField, Button } from '@material-ui/core';
// import {Container, Card, CardBody, makeStyles, Grid, TextField, Button} from 'reactstrap';
import QRCode from 'qrcode';
import QrReader from 'react-qr-reader';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { loginThunk } from '../redux/auth/thunk';
import { useDispatch } from 'react-redux';
import { useForm } from "react-hook-form";
import { Navigation } from '../layout/navigation';
import { loginSuccess } from '../redux/auth/action';
import QrScanner from 'qr-scanner'; // if installed via package and bundling with a module bundler like webpack or rollup



function QRcodeDemoLoginPage() {
    const [tokenQR, setTokenQR] = useState('');
    const [scanResult, setScanResult] = useState('');
    const [scanResultFile, setScanResultFile] = useState('');
    const [scanResultWebCam, setScanResultWebCam] = useState('');
    const classes = useStyles();
    const qrRef = useRef(null) as any;
    const dispatch = useDispatch()


    const token = useSelector(
        (state: IRootState) => state.auth.token
    )

    // const username = useSelector(
    //     (state: IRootState) => state.auth.user
    // )

    // const password = useSelector(
    //     (state: IRootState) => state.auth.
    // )

    // const generateQrCode = async () => {
    //     try {
    //         const response = await QRCode.toDataURL(tokenQR);
    //         setScanResult(response);
    //         console.log("scanResult: ", scanResult);
    //         return scanResult;
    //     } catch (error) {
    //         console.log(error);
    //     }
    // }

    // useEffect(() => {
    //     if (!token) {
    //         setTokenQR('');
    //     } else {
    //         setTokenQR(token);
    //         // generateQrCode()
    //     }

    // }, [])


    const handleErrorFile = (error: any) => {
        console.log(error);
    }
    // const handleScanFile = (result: any) => {
    //     if (result) {
    //         setScanResultFile(result);
    //     }
    // }

    const handleScanFile = (tokenQR: any) => {
        if (tokenQR) {
            setScanResultFile(tokenQR);
            return scanResultFile;
        }
    }

    // type FormState = {
    //     username: string,
    //     password: string
    // }

    // const { register, handleSubmit } = useForm<FormState>({
    //     defaultValues: {
    //         username: "",
    //         password: ""
    //     }
    // })

    // function onSubmit(state: FormState) {
    //     console.log('on from login :', state);
    //     dispatch(loginThunk(state))
    // }

    // useEffect(() => {

    //     generateQrCode();


    // }, [tokenQR])


    useEffect(() => {
        dispatch(loginSuccess(scanResultWebCam))
    }, [scanResultWebCam])

    const onScanFile = () => {
        qrRef!.current!.openImageDialog!();
    }

    const handleErrorWebCam = (error: any) => {
        console.log(error);
    }
    const handleScanWebCam = (result: any) => {
        if (result) {
            setScanResultWebCam(result);
        }
    }
    const handleOnload = ():void =>{
        console.log(('onloading'));        

    }

    // useEffect (() => {
    //     const qrScanner = new QrScanner(video, result => console.log('decoded qr code:', result));
    //     qrScanner.start();
    // }, [])

    return (
        <div>
            <Navigation />
            <Container className={classes.conatiner}>
                <Card>
                    <h2 className={classes.title}>Scan QR Code to Login in Mobile Web</h2>
                    <CardContent>
                        <Grid container spacing={2}>
                            <Grid item xl={4} lg={4} md={6} sm={12} xs={12}>
                                <br />
                                <br />
                                <br />
                                {scanResult ? (
                                    <a href={scanResult} download>
                                        <img src={scanResult} alt="img" />
                                    </a>) : null}
                            </Grid>
                            {/* <Grid item xl={4} lg={4} md={6} sm={12} xs={12}> */}
                                {/* <Button className={classes.btn} variant="contained" color="secondary" onClick={onScanFile}>Scan Qr Code</Button>
                                <QrReader
                                    ref={qrRef}
                                    delay={300}
                                    style={{ width: '100%' }}
                                    onError={handleErrorFile}
                                    onScan={handleScanFile}
                                    legacyMode
                                />
                                <h3>Scanned Code: {scanResultFile}</h3> */}
                            {/* </Grid> */}
                            <Grid item xl={4} lg={4} md={6} sm={12} xs={12}>
                                <p></p>
                                <p></p>
                                <h3>Qr Code Scan by Web Cam</h3>
                                <QrReader
                                    delay={300}
                                    style={{ width: '100%' }}
                                    onError={handleErrorWebCam}
                                    onScan={handleScanWebCam}
                                    onLoad={handleOnload}
                                />
                                <h3>Scanned By WebCam Code: {scanResultWebCam}</h3>
                            </Grid>

                            <video id="videoScan"></video>

                        </Grid>
                    </CardContent>
                </Card>
            </Container>
        </div>
    );
}

const useStyles = makeStyles((theme: any) => ({
    conatiner: {
        marginTop: 10
    },
    title: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: '#3f51b5',
        color: '#fff',
        padding: 20
    },
    btn: {
        display: 'flex',
        marginTop: 10,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
    }
}));
export default QRcodeDemoLoginPage;
