import React from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
// import { Form, Label, Button } from "reactstrap";
import {registerThunk} from '../redux/register/thunk';
import './Register.css'


export default function RegisterPage() {
  // const [username, setUsername] = useState('')
  // const [password, setPassword] = useState('')
  const dispatch = useDispatch()

  type FormState = {
    username: string,
    password: string
  }
  const {register, handleSubmit} = useForm <FormState> ({
    defaultValues: {
      username: "",
      password:""
    }
  })

  function onSubmit (state: FormState) {
    console.log('on from submit :', state);
    dispatch(registerThunk(state))
    
  }
  return (
    <div className="outer">
        <div className="inner">
          <form onSubmit={handleSubmit(onSubmit)}>

               <h3 style={{fontSize:'3rem', fontWeight:'bold'}}>Register</h3>
              
               <div className="form-group">
                   <label style={{fontSize:'2rem', marginTop:'8rem'}}>Username</label>
                   <input style={{fontSize:'2rem', marginTop:'1.8rem'}} type="text" className="form-control" placeholder="Enter username" {...register('username')} />
               </div>
               
               <div className="form-group">
                   <label style={{fontSize:'2rem', marginTop:'6rem'}}>Password</label>
                   <input style={{fontSize:'2rem', marginTop:'1.8rem'}} type="password" className="form-control" placeholder="Enter password" {...register('password')}/>
               </div>
               
               <button style={{fontSize:'2rem', marginTop:'8rem'}} type="submit" className="btn btn-dark btn-lg btn-block">Register</button>
               <p className="forgot-password text-right">
                  Already registered log in?
               </p>
          </form>
          {/* <Form onSubmit={handleSubmit(onSubmit)}>
            
            <Label> 
              username :
              <input type='text' {...register('username')}/>
            </Label>
            <Label> 
              password :
              <input type='text' {...register('password')}/>
            </Label>
            <Button type='submit'>
              submit 
            </Button>

          </Form> */}
        </div>
      </div>
    
    
    
    
  );
}

