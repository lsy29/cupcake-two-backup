import React, { useState, useRef, useEffect } from 'react';
import { Container, Card, CardContent, makeStyles, Grid, TextField, Button } from '@material-ui/core';
// import {Container, Card, CardBody, makeStyles, Grid, TextField, Button} from 'reactstrap';
import QRCode from 'qrcode';
import QrReader from 'react-qr-reader';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { Navigation } from '../layout/navigation';


function App() {
    const [tokenQR, setTokenQR] = useState('');
    const [scanResult, setScanResult] = useState('');
    const [scanResultFile, setScanResultFile] = useState('');
    const [scanResultWebCam, setScanResultWebCam] = useState('');
    const classes = useStyles();
    const qrRef = useRef(null) as any;


    const token = useSelector(
        (state: IRootState) => state.auth.token
    )

    const generateQrCode = async () => {
        try {
            const response = await QRCode.toDataURL(tokenQR);
            setScanResult(response);
            console.log("scanResult: ", scanResult);
            return scanResult;
        } catch (error) {
            console.log(error);
        }
    }     

    useEffect(() =>{
        if (!token){
            setTokenQR('');
        }else{
            setTokenQR(token);
            generateQrCode()
        }
        
    }, [])

    useEffect(() => {

        generateQrCode();
        
    }, [tokenQR])    

    const handleErrorFile = (error: any) => {
        console.log(error);
    }
    // const handleScanFile = (result: any) => {
    //     if (result) {
    //         setScanResultFile(result);
    //     }
    // }

    const handleScanFile = (tokenQR: any) => {
        if (tokenQR) {
            setScanResultFile(tokenQR);
            return scanResultFile;
        }
    }

    useEffect(() => {
        handleScanFile(tokenQR);
    }, [tokenQR])


    const onScanFile = () => {
        qrRef!.current!.openImageDialog!();
    }

    const handleErrorWebCam = (error: any) => {
        console.log(error);
    }
    const handleScanWebCam = (result: any) => {
        if (result) {
            setScanResultWebCam(result);
        }
    }

    return (
        <div>
            <Navigation/>
            <Container className={classes.conatiner}>
                <Card>
                    <h2 className={classes.title}>Scan QR Code to Login in Mobile Web</h2>
                    <CardContent>
                        <Grid container spacing={2}>
                            <Grid item xl={4} lg={4} md={6} sm={12} xs={12}>
                                {/* <TextField label="Enter Text Here" onChange={(e) => setTokenQR(e.target.value)} />
                                <Button className={classes.btn} variant="contained"
                                    color="primary" onClick={() => generateQrCode()}>Generate</Button> */}
                                <br />
                                <br />
                                <br />
                                {scanResult ? (
                                    <a href={scanResult} download>
                                        <img src={scanResult} alt="img" />
                                    </a>) : null}
                            </Grid>
                            {/* <Grid item xl={4} lg={4} md={6} sm={12} xs={12}> */}
                                {/* <Button className={classes.btn} variant="contained" color="secondary" onClick={onScanFile}>Scan Qr Code</Button> */}
                                {/* <Button className={classes.btn} variant="contained" color="secondary" onClick={onScanFile}>Scan Qr Code</Button>
                                <QrReader
                                    ref={qrRef}
                                    delay={300}
                                    style={{ width: '100%' }}
                                    onError={handleErrorFile}
                                    onScan={handleScanFile}
                                    legacyMode
                                /> */}
                                {/* <h3>Scanned Code: {scanResultFile}</h3> */}
                                <h3>Scanned Code: {scanResultFile}</h3>
                            {/* </Grid> */}
                            {/* <Grid item xl={4} lg={4} md={6} sm={12} xs={12}>
                         <h3>Qr Code Scan by Web Cam</h3>
                         <QrReader
                         delay={300}
                         style={{width: '100%'}}
                         onError={handleErrorWebCam}
                         onScan={handleScanWebCam}
                         />
                         <h3>Scanned By WebCam Code: {scanResultWebCam}</h3>
                      </Grid> */}
                        </Grid>
                    </CardContent>
                </Card>
            </Container>
        </div>
    );
}

const useStyles = makeStyles((theme: any) => ({
    conatiner: {
        marginTop: 10
    },
    title: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: '#3f51b5',
        color: '#fff',
        padding: 20
    },
    btn: {
        marginTop: 10,
        marginBottom: 20
    }
}));
export default App;
