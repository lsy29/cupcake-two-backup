import React, { useState, useEffect } from 'react'
import { Contact } from '../layout/footer'
import { Navigation } from '../layout/navigation'
import {
    Container, Row, Col, Card, CardText, CardBody, Button
} from 'reactstrap';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router'
// import { fetchVideos } from '../redux/video/thunk';
// import { videoReducer } from '../redux/video/reducer';
import { selectVideo } from '../redux/video/action';

const { REACT_APP_API_SERVER } = process.env


export default function VideoPageTesting() {
    const dispatch = useDispatch();
    const [Videos, setVideos] = useState<any[]>([])
    const selectedCatId = useSelector((state: IRootState) => state.video.selectedCat)

    useEffect(() => {
        // POST request using fetch inside useEffect React hook
        fetch(`${REACT_APP_API_SERVER}/video-byCat?cat=` + selectedCatId)
            .then(response => response.json())
            .then(data => setVideos(data));
    }, []);
    console.log("fetch videos", Videos)
    console.log("selectedCatId = ", selectedCatId)

    function onVideoClick(videoId: number) {
        dispatch(selectVideo(videoId))
        dispatch(push('/video-subtitle'))
    }

    // function rowHead(index: number) {
    //     if (index % 3 == 0) {
    //         return <Row></Row>
    //     }
    // }
    return (
        <div>
            <Navigation />
           
                <Container style={{ minHeight: "80vh" }}>

                    <div style={{ fontSize: "50px", marginTop: "40px", marginBottom: "30px" }}>
                        {Videos && Videos.length > 0 ? (Videos[0].video_genre) : ''}
                    </div>
                    <Row style={{ display: "flex", justifyContent: "center", flexWrap: "wrap" }}>
                        {Videos.map(video => (
                            <Col>
                                {/* <Card style={{ marginBottom: "60px" , width:"343px",}}> */}
                                <Card style={{ marginBottom: "60px", width: "342px", }}>

                                    <iframe title='video' src={"https://www.youtube.com/embed/" + video.youtube_video_id + "?controls=0"}
                                        width="342" height="193" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    ></iframe>
                                    <CardBody>
                                        <CardText>{video.video_title}</CardText>
                                        <Button style={{ fontSize: "1.3rem" }} onClick={() => onVideoClick(video.id)}>Click to see more</Button>
                                    </CardBody>
                                </Card>
                            </Col>
                        ))}
                    </Row>
=
                </Container>
            <Contact />
        </div>
    )
}


