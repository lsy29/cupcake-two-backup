import VideoBox from '../components/videoBox'
import { Contact } from '../layout/footer'
import { Navigation } from '../layout/navigation'
import { Container, Row, Col } from 'reactstrap';
import { useEffect, useState } from 'react';
import JsonData from '../data/data.json'


export default function VideoPage() {
  // const dataItem = {
  //   url:'https://www.youtube.com/embed/iNb7FdG9ucw?controls=0',
  //   title:'Demo props'
  // }

  const [videoItems, setVideoItems] = useState<any>({})
  useEffect(() => {
    setVideoItems(JsonData)
    }, [])
  console.log('landingPageData=', videoItems);

  // function rowHead(index:number){
  //     if (index % 3 === 0 ){
  //       return <Row></Row>
  //     }
  // }
  return (
    <div>
      <Navigation />
      <Container>
      <h1>
        Travel
      </h1>
      <Row>
      {videoItems.videoBox ? videoItems.videoBox.map((videoItem : any , videoIndex : number)=>{
        return (
        <>
          {/* {1+1 ==2? <Row> : '' } */}
          <Col><VideoBox dataItem={videoItem}/></Col>
          {/* {videoIndex % 3 ==0 ? <Row></Row> :''} */}
        </>
        )
      }) : ''
    }
    </Row>

      <br></br>
      <br></br>
      <br></br>

      </Container>
     
      <Contact/>
    </div>
  )
}

