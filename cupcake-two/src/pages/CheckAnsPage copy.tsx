import { Contact } from '../layout/footer'
import { Navigation } from '../layout/navigation'
import {
  Container, Col, Row, Jumbotron, Button, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, CardHeader, CardFooter
} from 'reactstrap';
import { useDispatch } from 'react-redux';
import AnsComparison from '../components/ansComparison';
import ExAndQuestion from '../components/exAndQuestion';
import UserAndVideo from '../components/userAndVideo';


export default function CheckAnsPage() {
  const dispatch = useDispatch();
  return (
    <div>
      <Navigation />
      <Container style={{ width: "950px" }}>
        <UserAndVideo />
        <AnsComparison />
        <ExAndQuestion />
      </Container>
      <Contact />

    </div>
  )
}

