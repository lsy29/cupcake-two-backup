import QrScanner from "qr-scanner";
// import "./Qrscanner.scss";
import { useDispatch } from "react-redux";
// import { getClientInfoThunk } from "../redux/profileLookUp/thunk";
import { useHistory } from "react-router";
import { useEffect, useState } from "react";

function ReactQrScanner(props: {
    onQrCode: (qrCode: string) => void;
    hidden?: boolean;
}) {
    const [video, setVideo] = useState<HTMLVideoElement | null>(null);

    useEffect(() => {
        console.log("useEffect video:", video);

        if (!video) {
            return;
        }
        let streamPromise = navigator.mediaDevices.getUserMedia({
            video: true,
            audio: false,
        });
        streamPromise.catch((err) => {
            console.error("failed to get user media for qr scanner:", err);
        });
        let qrScannerPromise = streamPromise.then((stream) => {
            if (!video) {
                return;
            }
            return new Promise<QrScanner>((resolve) => {
                video.srcObject = stream;
                video.onloadedmetadata = () => {
                    video.play();
                    let qrScanner = new QrScanner(video, (result: any) => {
                        console.log("result", result);
                        let resultObj = JSON.parse(result);
                        props.onQrCode(resultObj.Token);
                        qrScanner.stop();
                    });
                    qrScanner.start();
                    resolve(qrScanner);
                };
            });
        });

        async function stop() {
            console.log("clean up video:", video);
            streamPromise.then((stream) => {
                stream.getVideoTracks().forEach((track) => stream.removeTrack(track));
            });
            qrScannerPromise.then(async (qrScanner) => {
                if (!qrScanner) {
                    console.log("no scarner D");

                    return;
                }
                console.log("stoping scaner :", qrScanner);

                await qrScanner.stop();
                await qrScanner.destroy();
                console.log("destroyed :", qrScanner);
            });
        }

        // clean up
        return () => {
            console.log("child clear up");

            stop();
        };
    }, [video, props]);

    return (<video ref={setVideo} hidden={props.hidden} />)
}


    function QrCodeScanner(props: any) {
        const [qrCode, setQrCode] = useState("");
        const [shouldScan, setShouldScan] = useState(false);
        const dispatch = useDispatch();
        const history = useHistory();

        function startScanner() {
            setQrCode("");
            setShouldScan(true);
        }
        function stopScanner() {
            setQrCode("");
            setShouldScan(false);
        }
        function onQRCode(qrCode: string) {
            // setShouldScan(false)
            setQrCode(qrCode);

            // requestAnimationFrame(() => setShouldScan(true))
            // dispatch(getClientInfoThunk(qrCode, history));
        }


        return (
            <div>
                <button style={{ color: "red" }} onClick={startScanner} />
Scan QR Code
                <button className="QrCodeScanner" onClick={stopScanner}>
                    quit
  </button>
                {shouldScan && <ReactQrScanner onQrCode={onQRCode} />}
                <p>
                    qr code: <code>{qrCode}</code>
                </p>
            </div>
        );
    }

    export default QrCodeScanner;