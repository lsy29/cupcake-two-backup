// import './memo.scss'
import React, { useState , useEffect ,FormEvent} from 'react'
import { i2w } from './img2wordType'
import { api, REACT_APP_API_SERVER } from '../../helpers/api'
import Swal from 'sweetalert2'

import {sayHelloThunk , postImgThunk , dtImgObjThunk} from "../../redux/img2word/thunk"


import { useDispatch, useSelector } from 'react-redux'

import {IRootState} from '../../redux/store'

import {useForm} from 'react-hook-form'

type Props = {
//   memo: Memos[number]
}



export function Dtbox() {

  const dispatch = useDispatch();


  function dtImgObj(e:FormEvent){
    e.preventDefault()
    const postImgForm = e.target as HTMLFormElement
    console.log(postImgForm)
    dispatch(dtImgObjThunk(postImgForm))
    // dispatch(postImgThunk(postImgForm))
  }

    const [state, setState] = useState(0)
    useEffect(() => {
      let timer = setTimeout(() => {
        setState(state => state + 1)
      }, 1000)
      return () => {
        clearTimeout(timer)
      }
    }, [state])
  



    // const imgpath = useSelector (
    //     (state:IRootState) => state.img2word.i2wObject?.image
    //   )
    const pyimgpath = useSelector (
      (state:IRootState) => state.img2word.i2wObject?.py_image
    )

    return (
    <>
    <div>Object Detection</div>
    <span>{state}</span>
    <br></br>
    <img src={pyimgpath}></img>
    <div>{pyimgpath}</div>




    <br></br>
    <br></br>
    <br></br>
    {/* <button onClick={previewImg}>Preview the Image</button> */}
    </>
    )
}

export default Dtbox;