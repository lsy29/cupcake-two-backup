export declare type i2w = {
    id : number;
    title : string;
    image : string;
    py_image : string;
    dtwords : Array<string>;

};