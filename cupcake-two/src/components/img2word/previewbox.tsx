// import './memo.scss'
import React, { useState , useEffect ,FormEvent} from 'react'
import { i2w } from './img2wordType'
import { api, REACT_APP_API_SERVER } from '../../helpers/api'
import Swal from 'sweetalert2'

import {sayHelloThunk , postImgThunk , dtImgObjThunk} from "../../redux/img2word/thunk"


import { useDispatch, useSelector } from 'react-redux'

import {IRootState} from '../../redux/store'

import {useForm} from 'react-hook-form'

type Props = {
//   memo: Memos[number]
}



export function Pvbox() {

  const dispatch = useDispatch();


  function dtImgObj(e:FormEvent){
    e.preventDefault()
    const postImgForm = e.target as HTMLFormElement
    console.log('Current directory: ' + process.cwd());
    console.log(postImgForm)
    dispatch(dtImgObjThunk(postImgForm))
    // dispatch(postImgThunk(postImgForm))
  }

    const [state, setState] = useState(0)
    useEffect(() => {
      let timer = setTimeout(() => {
        setState(state => state + 1)
      }, 1000)
      return () => {
        clearTimeout(timer)
      }
    }, [state])
  
    const imgpath = useSelector (
      (state:IRootState) => state.img2word.i2wObject?.image
    )

    return (
    <>
    <div style={{backgroundColor:"pink"}} >Image Display</div>
    {/* <span>{state}</span> */}
    <br></br>
    {/* <div>{imgpath}</div> */}
    <form
        id="post-imgpath"
        method="POST"
        action="/dtimgobj"
        encType="multipart/form-data"
        onSubmit={dtImgObj}
    >

      <input id="express_imgpath" name="express_imgpath" type="hidden" value={imgpath}></input>
      <input type="submit" value="Detecting Objects" />

    </form>

    <div >

    <img style={{width:"100%"}} src={imgpath}></img>

    </div>

    <br></br>
    <br></br>
    <br></br>
    {/* <button onClick={previewImg}>Preview the Image</button> */}
    </>
    )
}

export default Pvbox;