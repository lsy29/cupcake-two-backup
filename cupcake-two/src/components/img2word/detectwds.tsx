// import './memo.scss'
import React, { useState , useEffect ,FormEvent} from 'react'
import { i2w } from './img2wordType'
import { api, REACT_APP_API_SERVER } from '../../helpers/api'
import Swal from 'sweetalert2'

import {imgDictThunk} from "../../redux/img2word/thunk"


import { useDispatch, useSelector } from 'react-redux'

import {IRootState} from '../../redux/store'

import {useForm} from 'react-hook-form'

import './detectwds.css'
import { searchVocabThunk } from '../../redux/notebook/thunk'
import DictionaryResult from '../DictionaryResult'

type Props = {
//   memo: Memos[number]
}



export function Dtwds(props: { clickToOpen: string, setClickToOpen: (clickToOpen: string | null) => void}) {

  const dispatch = useDispatch();

  const dtwarray = useSelector (
    (state:IRootState) => state.img2word.i2wObject?.dtwords
  )

  function imgDict(e:FormEvent){
    
    e.preventDefault()
    const postWordForm = e.target as HTMLFormElement
    // console.log('Current directory: ' + process.cwd());
    console.log("dt word$$$ : : : ",postWordForm)
    // dispatch(imgDictThunk(postWordForm))
 
  }

  function Wordlink(){
    const dtwarray = useSelector (
      (state:IRootState) => state.img2word.i2wObject?.dtwords
    )
    function Resultedlinks(word:any){
      word=word.slice(1, -1)
      
      return(
        <>
        <form
            id="post-imgpath"
            method="POST"
            action="/dtimgobj"
            encType="multipart/form-data"
            onSubmit={imgDict}
            onClick={() => resultWordClicked(word)}
        >


          <input id="img-detected-word" name="img-detected-word" type="hidden" value={word}></input>
          <input type="submit" className="submitlink" value={word} />

        </form>


        </>
      )

      
    }

    function resultWordClicked(word: string) {
      console.log("wordClicked:", word);
      dispatch(searchVocabThunk(word))
      props.setClickToOpen(word)
      console.log("clickToOpen from detectwds:", word)
    }
 


    return dtwarray?.map(word=>Resultedlinks(word))
    

  }



    






    return (
    <>
    <div style={{backgroundColor:"skyblue"}}>Detected Words</div>
    <br></br>
    <span>{Wordlink()}</span>
    <br></br>




    </>
    )
}

export default Dtwds;