import {
    Container, Col, Row, Jumbotron, Button, Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, CardHeader, CardFooter
} from 'reactstrap';

export default function AnsComparison() {
    return (
        <div>
            <Container style={{ width: "700px" }}>
                <Row>
                    <Col> <Card style={{
                        width: "300px", height: "175px", display: "flex", borderRadius: "15px"}} >
                        <CardHeader tag="h3" style={{ display: "flex", alignItems: "center", justifyContent: "center", 
                        backgroundColor: "aliceBlue", borderTopLeftRadius: "15px", borderTopRightRadius: "15px" }}>Your Answer</CardHeader>
                        <CardBody>
                            <CardText>1.<br />2.<br />3.<br />4.<br />5.</CardText>
                        </CardBody>
                    </Card></Col>
                    <Col> <Card style={{
                        width: "300px", height: "175px", display: "flex", borderRadius: "15px"}} >
                        <CardHeader tag="h3" style={{ display: "flex", alignItems: "center", justifyContent: "center", 
                        backgroundColor: "lavender", borderTopLeftRadius: "15px", borderTopRightRadius: "15px" }}>Correct Answer</CardHeader>
                        <CardBody>
                            <CardText>1.<br />2.<br />3.<br />4.<br />5.</CardText>
                        </CardBody>
                    </Card></Col>
                </Row>
            </Container>
        </div>
    );
}

