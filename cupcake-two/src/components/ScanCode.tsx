import React, { useState, useRef, useEffect } from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button, Container
} from 'reactstrap';
import '../components/QRcode.css'
import { BiLogOut } from 'react-icons/bi';
import QrReader from 'react-qr-reader';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { useDispatch } from 'react-redux';
import { useForm } from "react-hook-form";
import { Navigation } from '../layout/navigation';
import { loginSuccess } from '../redux/auth/action';
import QrScanner from 'qr-scanner'; // if installed via package and bundling with a module bundler like webpack or rollup
import Swal from 'sweetalert2';
import { push } from 'connected-react-router';
import { history } from '../redux/store';
import Logo from '../layout/CUPCAKE.gif';






export default function ScanCode() {
  const [darkMode, setDarkMode] = useState(false);

  const [tokenQR, setTokenQR] = useState('');
  const [scanResult, setScanResult] = useState('');
  const [scanResultFile, setScanResultFile] = useState('');
  const [scanResultWebCam, setScanResultWebCam] = useState('');

  const qrRef = useRef(null) as any;
  const dispatch = useDispatch()


  const token = useSelector(
    (state: IRootState) => state.auth.token
  )



  const handleErrorFile = (error: any) => {
    console.log(error);
  }
  // const handleScanFile = (result: any) => {
  //     if (result) {
  //         setScanResultFile(result);
  //     }
  // }

  const handleScanFile = (tokenQR: any) => {
    if (tokenQR) {
      setScanResultFile(tokenQR);
      return scanResultFile;
    }
  }


  useEffect(() => {
    dispatch(loginSuccess(scanResultWebCam))
    if (token) {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Login Success',
        showConfirmButton: false,
        timer: 1500
      })
      history.push('/')
    }

  }, [scanResultWebCam, token])

  const onScanFile = () => {
    qrRef!.current!.openImageDialog!();
  }

  const handleErrorWebCam = (error: any) => {
    console.log(error);
  }
  const handleScanWebCam = (result: any) => {
    if (result) {
      setScanResultWebCam(result);
    }
  }
  const handleOnload = (): void => {
    console.log(('onloading'));

  }


  return (

    <div className="BGbox">
      {/* <div className={darkMode ? "dark-mode" : "light-mode"}> */}
      <a href="/">
        <img className="cupcakeLogo" src={Logo} style={{ width: "70px", height: "70px" }} />
      </a>
      <div className="toggle-container">
        {/* <span style={{ color: darkMode ? "grey" : "yellow" }}>☀︎</span> */}
        <div className="switch-checkbox">
          {/* <label className="switch">
            <input type="checkbox" onChange={() => setDarkMode(!darkMode)} />
            <span className="slider round"> </span>
          </label> */}
        </div>
        {/* <span style={{ color: darkMode ? "#c96dfd" : "grey" }}>☽</span> */}
      </div>
      {/* <div>
        <h1>Cool its {darkMode ? "Dark" : "Light"} Mode </h1>
      </div> */}







      <Card className="box-container">


        <QrReader className="box-img"
          delay={300}
          style={{ width: "40%" }}
          onError={handleErrorWebCam}
          onScan={handleScanWebCam}
          onLoad={handleOnload}
        />


        <CardBody className="box-main" >
          <CardTitle className="box-title">QR code </CardTitle>

          <CardText className="box-step">Step 1: Open your scanner on your phone </CardText>
          <CardText className="box-step">Step 2: Scan the QR code </CardText>
          <CardText className="box-step">Step 3: Done</CardText>
          <a href="/">
            <BiLogOut className="box-icon" />
          </a>
          {/* <CardText>Back to Home</CardText> */}
        </CardBody>
      </Card>


    </div>
  );
}









