// import {DictionaryIcon} from './DictionaryIcon'
import DictionarySearch from './DictionarySearch'
export const Header = (props) => {
  return (
    <header id='header'>
      <div className='intro'>
        <div className='overlay'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-8 col-md-offset-2 intro-text'>
                <h1 style={{fontSize:'6.5rem'}}>
                  {props.data ? props.data.title : 'Loading'}
                  <span ></span>
                </h1>
                <p style={{fontSize:'2.5rem', marginTop:'3rem'}}>{props.data ? props.data.paragraph : 'Loading'}</p>
                <a
                  href='#features'
                  className='btn btn-custom btn-lg page-scroll'
                  style={{fontSize:"1.8rem", marginTop:'5rem'}}
                >
                  Find Out More
                </a>{' '}
              </div>
            </div>
          </div>
        </div>
      </div>
      <DictionarySearch/>
    </header>
  )
}
