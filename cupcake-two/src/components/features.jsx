import React from 'react';
import { Container, Row, Col } from 'reactstrap';

export const Features = (props) => {
  return (
    <div id='features' className='text-center' >
      
      <div className="container">

        <Row >
          {props.data
            ? props.data.map((dataItem, dataIndex) => (
              <div key={`${dataItem.title}-${dataIndex}`} className='col-xs-6 col-md-3' >
                {' '}
                <i style={{ width: "15rem", height: "15rem", display: 'flex', alignItems: 'center', justifyContent: 'center', fontSize: '8rem' }} className={dataItem.icon}></i>
                <h3 style={{ fontSize: "2.5rem", marginRight:"10rem", marginBottom:'2rem'}}>{dataItem.title}</h3>
                <p style={{ fontSize: "1.8rem", marginRight:"10rem"}}>{dataItem.text}</p>
              </div>
            ))
            : 'Loading...'}

        </Row>
      </div>
    </div>
  )
}
