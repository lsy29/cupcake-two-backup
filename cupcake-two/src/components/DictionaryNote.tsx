import { Navigation } from '../layout/navigation'
import './DictionaryNote.css'
import { Contact } from '../layout/footer'
import {
    Card, Button, CardHeader, CardFooter, CardBody,
    CardTitle, CardText, Container
} from 'reactstrap';
import { ImBin } from 'react-icons/im';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteVocabThunk, loadAllMyVocabThunk } from '../redux/myVocab/thunk';
// import { isNotEmittedStatement } from 'typescript';
import { IRootState } from '../redux/store';
// import { AiTwotoneSound } from 'react-icons/ai';
import DictionaryResult from './DictionaryResult';
// import ReactCardFlip from "react-card-flip";
import Swal from 'sweetalert2';



export default function DictionaryNote() {

    const dispatch = useDispatch();

    // const [myVocabs, setMyVocabs] = useState<any>({})
    // const selectedDeleId = useSelector((state:IRootState)=> state.myVocab.vocabId)
    const words = useSelector((state: IRootState) => state.myVocab.words)

    console.log("word from state: ", words);
    

   
    console.log("word from state: ", words);

    useEffect(() => {
        // let async getResult = await loadAllMyVocabThunk()
        //             .then(response => response.json())
        //             .then(result => setMyVocabs(result))
        dispatch(loadAllMyVocabThunk())

    }, [])

    function onDelete(noteId: any) {
        dispatch(deleteVocabThunk(noteId))
        Swal.fire({
            position: 'top-end',
            title: 'Word has been deleted',
            showConfirmButton: false,
            timer: 1500
          })


        
    }

    return (
        <div>
            <Navigation />
            <h1 style={{fontSize:"5rem", marginTop:"2%",textAlign:"center", fontWeight:"bold"}}>My Vocab</h1>
            {/* <Container style={{height:"calc(100vh - 92px - 212px)"}}> */}
            <div style={{height:"100vh", overflow:"auto", display:"flex", justifyContent:"center"}}>
                <div className="notebook-container">
                    {words?.map((myVocabItems) => {
                        return (
                            <Card className="notebook-whole" key={myVocabItems.id} style={{ width: "25rem", marginTop: "5rem", display: "flex" }}>
                                <CardHeader className="notebook-header"> {myVocabItems.word}
                                    <ImBin className="deleteBTN" onClick={() => onDelete(myVocabItems.id)} />

                                </CardHeader>

                                <DictionaryResult word = {myVocabItems}/>
                                {console.log("DictionaryResult component executed")}

                            </Card>

                        )
                    })}


                    {/* card template*/}
                    {/* <Card style={{ width: "25rem", marginTop:"5rem", display: "flex"}}>
                    <CardHeader>Header</CardHeader>
                    <CardBody>
                        <CardTitle tag="h5">Special Title Treatment</CardTitle>
                        <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                        <Button>Go somewhere</Button>
                    </CardBody>
                    <CardFooter>Footer</CardFooter>
                </Card> */}

                </div>
            </div>
            <Contact />

        </div>
    )
}