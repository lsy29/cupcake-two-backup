import { useState } from 'react';
import './DictionaryIcon.css';
import { ImBook } from 'react-icons/im';
import { ImSearch } from 'react-icons/im';
import { AiFillPlusCircle } from 'react-icons/ai';
import { GiSpellBook } from 'react-icons/gi';
import { useDispatch, useSelector } from 'react-redux';
// import { searchMyVocabThunk } from '../redux/myVocab/thunk'
import { addVocabThunk, searchVocabThunk } from '../redux/notebook/thunk'
import { IRootState, IRootThunkDispatch } from '../redux/store';
import DictionaryResult from './DictionaryResult'
import Swal from 'sweetalert2';
import { autoLoginThunk } from '../redux/auth/thunk';
import { useEffect } from 'react';
import QRCode from 'qrcode';
import { Search } from 'react-bootstrap-icons';
import { BookHalf } from 'react-bootstrap-icons';
import { PlusCircleFill } from 'react-bootstrap-icons';
import userEvent from '@testing-library/user-event';
// import ReactLoading from 'react-loading';


export default function DictionarySearch() {

    const [isOpen, setIsOpen] = useState(true)
    const dispatch = useDispatch()
    const [searchWordValue, setSearchWordValue] = useState('');
    const [scanResult, setScanResult] = useState('');
    const [searchBTNClicked, setSearchBTNClicked] = useState(false);
    const [scanResultFile, setScanResultFile] = useState('');

    const Loading = require('react-loading-animation');


    const info = useSelector(

        (state: IRootState) => state.auth.user

    )

    // const mainWord = useSelector(
    //     (state: IRootState) => state.notebook.word
    // )    

    const word = useSelector(
        // (state: IRootState) => state.myVocab.words
        (state: IRootState) => state.notebook.word
    )

    // const loading = () => (   //{ "type": "cubes", "color": "#39A2DB" }
    // <ReactLoading type={'cubes'} color={'#39A2DB'} height={'20%'} width={'20%'} />
    // );

    const generateQrCode = async () => {

        if (!word) {
            return
        } else {
            try {
                const response = await QRCode.toDataURL(word?.word!);
                setScanResult(response);
                console.log("scanResult: ", scanResult);
                return scanResult;
            } catch (error) {
                console.log(error);
            }
        }


    }

    // let all = word?.id! + "+" + word?.word! + word?.phonetics! + "+" + word?.meanings!;

    useEffect(() => {

        generateQrCode();
        setSearchBTNClicked(false)

    }, [word])

    

    function searchWord() {
        let searchWord: string = (document.querySelector('#searchWord') as any).value
        // dispatch(searchMyVocabThunk(searchWord))
        dispatch(searchVocabThunk(searchWord))
        setSearchBTNClicked(true);
    }

    function addCard() {
        dispatch(addVocabThunk(info!.id!, word!.id?.toString()))
        Swal.fire({
            position: 'top-end',
            title: 'Word has been saved',
            showConfirmButton: false,
            timer: 1500
        })

    }

    useEffect(() => {
        if (!info)
            dispatch(autoLoginThunk())

    }, [info])

    // console.log("info id: ", info!.id!)

    return (
        <div>
            <span id="chat-box" onClick={() => setIsOpen(!isOpen)} >
                <ImBook className="ImBook" />
                {/* <ImBook /> */}
            </span>


            {!isOpen && <div className="chat-popup" id='dict-popup' >

                <div className="input-group" />
                <div className="form-outline">
                    <form id="search">
                        <div className="search-bar-top">
                            <input type="search"
                                id="searchWord"
                                name="searchWord"
                                className="form-control"
                                placeholder="Enter a word in here"
                                style={{ height: "3rem", fontSize: "2rem", padding: "0.3rem", textAlign: "center" }} />
                            <label className="form-label" ></label>
                        </div>
                        <button
                            type="button"
                            id="dict-search"
                            className="btn btn-dictionary"
                            // className="fas fa-search"
                            onClick={() => searchWord()}

                        >
                            {/* <i className="fas fa-search"></i> */}
                            {/* <ImSearch /> */}
                            <Search />
                        </button>
                    </form>
                </div>

                {searchBTNClicked ? <Loading/> : 

                <div>

                {word ? <div className="plus-circle">
                    <PlusCircleFill
                        style={{ cursor: "pointer" }}
                        onClick={() => addCard()}
                    />
                </div> : ""}

                {console.log("search word: ", word?.word)}

                <div className="dicResultContrainer" style={{ display: "flex", justifyContent: "space-around", alignItems: "center"}}>
                    <div className="plus-circle" style={{ padding: "1.5rem"}} onClick={() => { }}> {word?.word}</div>

                    {scanResult ? (
                        <a href={scanResult} download>
                            <img style={{ width: "45%", marginLeft: "7rem" }} src={scanResult} alt="img" />
                        </a>) : null}

                </div>

                {word ? <DictionaryResult word={word} />
                    :
                    <span>
                        <div id="dict-result"></div>
                        {/* <i className="fas fa-book-open"></i>  */}
                        <div className='beforeSearch'>
                            {/* <GiSpellBook /> */}
                            <BookHalf />
                            <p className="dict-text">Let Start Your Learning Journey</p>
                        </div>
                    </span>
                }
            </div>

            }
            </div>
            }

        </div>
    )

}
