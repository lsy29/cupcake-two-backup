import { Navigation } from '../layout/navigation'
import './DictionaryNote.css'
import { Contact } from '../layout/footer'
import {
    Card, Button, CardHeader, CardFooter, CardBody,
    CardTitle, CardText, Container
} from 'reactstrap';
import { ImBin } from 'react-icons/im';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteVocabThunk, loadAllMyVocabThunk } from '../redux/myVocab/thunk';
// import { isNotEmittedStatement } from 'typescript';
import { IRootState, IRootThunkDispatch } from '../redux/store';
import ReactCardFlip from "react-card-flip";



export default function DictionaryNote() {

    const dispatch = useDispatch();
    const [isFlipped, setIsFlipped] = useState(false);

    const handleClick = () => {
        setIsFlipped(!isFlipped);
    };

    // const [myVocabs, setMyVocabs] = useState<any>({})
    // const selectedDeleId = useSelector((state:IRootState)=> state.myVocab.vocabId)
    const words = useSelector((state: IRootState) => state.myVocab.words)

    console.log("word from state: ", words);


    useEffect(() => {
        // let async getResult = await loadAllMyVocabThunk()
        //             .then(response => response.json())
        //             .then(result => setMyVocabs(result))
        dispatch(loadAllMyVocabThunk())

    }, [])



    // const myNotes = [

    // {
    //     id:0,
    //     word:'tree',
    //     meaning:'jjsdjs'
    // },{
    //     id:1,
    //     word:'tree',
    //     meaning:'jjsdjs'
    // }


    // ]

    function onDelete(noteId: any) {
        dispatch(deleteVocabThunk(noteId))
    }
    return (
        <div>
            {/* {myVocabs? myVocabs.map((noteItem)=>{
<card id={isNotEmittedStatement.id} >
    <div></div>
    <button onClick={onDelete(noteItem.id)}></button>
</card>
}):""} */}

            <Navigation />
            <Container >
                <div className="notebook-container">
                    {words?.map((myVocabItems) => {

                        return (

                            <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal" >
                                
                                <div className="notebook-whole"
                                
                                    style={{
                                        fontSize:"3rem",
                                        height: "25rem",
                                        color: "#000",
                                        display: "flex",
                                        flexDirection:"column",
                                        justifyContent: "center",
                                        alignItems: "center"
                                    }}
                                    onClick={handleClick}
                                >
                                    <ImBin onClick={ () => onDelete(myVocabItems.id)}
                                  

                                    
                                    />
                                      {myVocabItems.word}
                                    
                                </div>

                                
                                
                                
                               


                                <div className="notebook-whole"
                                    style={{
                                        backgroundColor:"yellowgreen",
                                        fontSize:"3rem",
                                        height: "25rem",
                                        color: "#000",
                                        display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center"
                                    }}
                                    onClick={handleClick}

                                >
                                    Create video tutorials on YouTube
                                
                                </div>
                               
                            </ReactCardFlip>















                            // <Card className="notebook-whole"

                            //       key={myVocabItems.id} 
                            //       style={{ width: "25rem", marginTop:"5rem", display: "flex"}}>
                            //     <CardHeader className="notebook-header"> {myVocabItems.word}
                            //     <ImBin onClick={ () => onDelete(myVocabItems.id)}/>

                            //     </CardHeader>
                            //     <CardBody >
                            //         <CardText onClick={handleClick} className="front">
                            //             <div>
                            //             {myVocabItems.word}
                            //                 {/* {myVocabItems.phonetics?.map()} */}
                            //             </div>
                            //         </CardText>

                            //         <CardText className="back">
                            //             <div>
                            //             jjjjjjjj
                            //                 {/* {myVocabItems.phonetics?.map()} */}
                            //             </div>
                            //         </CardText>

                            //     </CardBody>


                            // </Card>  
                        )
                    })}



                    <Card style={{ width: "25rem", marginTop: "5rem", display: "flex" }}>
                        <CardHeader>Header</CardHeader>
                        <CardBody>
                            <CardTitle tag="h5">Special Title Treatment</CardTitle>
                            <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                            <Button>Go somewhere</Button>
                        </CardBody>
                        <CardFooter>Footer</CardFooter>
                    </Card>

                </div>
            </Container>
            <Contact />

        </div>
    )
}