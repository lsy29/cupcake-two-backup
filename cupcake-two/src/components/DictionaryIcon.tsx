import { useState, useEffect } from 'react';
import './DictionaryIcon.css';
import { ImBook } from 'react-icons/im';
import { ImSearch } from 'react-icons/im';
import { AiFillPlusCircle } from 'react-icons/ai';
import { AiTwotoneSound } from 'react-icons/ai';
import { GiSpellBook } from 'react-icons/gi';
import { useDispatch, useSelector } from 'react-redux';
import { searchVocabThunk, addVocabThunk } from '../redux/notebook/thunk'
import { IRootState } from '../redux/store';
import { autoLoginThunk } from '../redux/auth/thunk';
import Swal from 'sweetalert2'


export function DictionaryIcon() {
    const [isOpen, setIsOpen] = useState(true)
    const dispatch = useDispatch()

    const info = useSelector(
        
        (state: IRootState) => state.auth.user
        
    )
    

    const word = useSelector(
        (state: IRootState) => state.notebook.word
    )

    

    function searchWord() {
        let searchWord: string = (document.querySelector('#searchWord') as any).value
        dispatch(searchVocabThunk(searchWord))
    }

    function addCard() {
        dispatch(addVocabThunk(info!.id!, word!.id?.toString()))
        Swal.fire({
            position: 'top-end',
            title: 'Word has been saved',
            showConfirmButton: false,
            timer: 1500
          })
        
    }


    useEffect(() => {
        if (!info)
            dispatch(autoLoginThunk())

    }, [info])

    


    return (
        <div>
            <span id="chat-box" onClick={() => setIsOpen(!isOpen)} >
                <ImBook />
            </span>


            {!isOpen && <div className="chat-popup" id='dict-popup' >

                <div className="input-group" />
                <div className="form-outline">
                    <form id="search">
                        <div className="search-bar-top">
                            <input type="search"
                                id="searchWord"
                                name="searchWord"
                                className="form-control"
                                placeholder="Enter a word in here"
                                style={{ height: "3rem", fontSize: "2rem", padding: "0.3rem", textAlign: "center" }} />
                            <label className="form-label" ></label>
                        </div>
                        <button
                            type="button"
                            id="dict-search"
                            className="btn btn-dictionary"
                            style={{ height: "3rem" }}
                            onClick={() => searchWord()}>
                            <ImSearch />
                        </button>
                    </form>
                </div>

                <div className="plus-circle">
                    <AiFillPlusCircle
                        style={{ cursor: "pointer" }}
                        onClick={() => addCard()} />
                </div>


                <div id="add-to-note" className="icon-container-outer">
                    <div className="plus-circle" > {word?.word}</div>


                    {
                        word && Object.keys(word).length > 0 ?

                            <div>


                                <div>phonetic length = {(word.phonetics?.length)}</div>

                                {word.phonetics && word.phonetics.length > 0 ? word.phonetics!.map((phonetic: any) =>

                                    <div>
                                        <div style={{ cursor: "pointer" }}
                                            className="plus-circle"
                                            onClick={() => {
                                                let audio = new Audio(phonetic.audio)
                                                audio.play()
                                            }}>
                                            {phonetic.text}
                                            <AiTwotoneSound />
                                        </div>


                                        {/* <b>{phonetic.audio}</b> */}
                                    </div>
                                )
                                    : 'no meaning'

                                }


                                {/* <hr style={{ width: "100%", height:"0.2rem"}} /> */}


                                <div >

                                    {/* <div>meaning length = {(word.meanings?.length)}</div> */}

                                    {word.meanings && word.meanings.length > 0 ? word.meanings!.map((meaning: any) =>

                                        <div className="word-definition">
                                            <div >@@@@@@{(meaning.definitions.length)}</div>

                                            {meaning.definitions.map((definition: any) =>
                                                <p style={{ marginTop: "0.1rem" }}>
                                                    {definition.definition} 
                                                    
                                                </p> 

                        
                                            )}


                                              {meaning.definitions.map((definition: any) =>
                                              <p style={{ marginTop: "0.1rem", border:"solid 2px black"}}> 
                                                <p >
                                                    {definition.example} 
                                                </p> 

                                              </p>
                                                




                                  

                                            )}
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            </div>
                                    )
                                        : 'no meaning'

                                    }



                                </div>









                            </div>


                            : <span>
                                <div id="dict-result"></div>
                                {/* <i className="fas fa-book-open"></i>  */}
                                <div className='beforeSearch'>
                                    <GiSpellBook />
                                    <p className="dict-text">Let Start Your Learning Journey</p>
                                </div>
                            </span>
                    }

                </div>


            </div>}


        </div>)

}


