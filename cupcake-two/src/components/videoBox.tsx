import React from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';
import { useDispatch } from 'react-redux';
import {push} from 'connected-react-router'

const VideoBox = (props :any) => {
  const dispatch = useDispatch();
 let {youtube_video_id , title } = props.dataItem

  console.log('From video box , url = ', youtube_video_id);
  console.log('From video box , title = ', title);

  return (
    <div>
      <Card style={{marginBottom:"60px"}}>
        <iframe src={"https://www.youtube.com/embed/" + youtube_video_id + "?controls=0"}
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          ></iframe>
        <CardBody>
          <CardText>{title}</CardText>
          <Button onClick={() => dispatch(push('/video-subtitle'))}>Click to see more</Button>
        </CardBody>
      </Card>
      {/* testing */}
    </div>
  );
};

export default VideoBox;