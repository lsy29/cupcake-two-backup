import React from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import {push} from 'connected-react-router'
import { IRootState } from '../redux/store';
import { Link } from 'react-router-dom';



// const id = useSelector((state: IRootState) => state.video.selectedID)

// console.log("video id: ", id);


// function getVideo(videoId: number) {
//     dispatchEvent()
// }


const VideoBox = (props :any) => {
  const dispatch = useDispatch();
 let {youtube_video_id , title } = props.dataItem

  console.log('From video box , url = ', youtube_video_id);
  console.log('From video box , title = ', title);

  return (
    <div>
      <Card style={{marginBottom:"60px"}}>
        <iframe src={"https://www.youtube.com/embed/" + youtube_video_id + "?controls=0"}
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          ></iframe>
        <CardBody>
          <CardText>{youtube_video_id}</CardText>
          <Link to={"/video-subtitle/" + youtube_video_id}><Button>Click to see more</Button></Link>
        </CardBody>
      </Card>
      {/* testing */}
    </div>
  );
};

export default VideoBox;