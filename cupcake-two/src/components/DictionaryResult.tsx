import './DictionaryResult.css'
import {
    Card, CardBody,
    CardText
} from 'reactstrap';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { loadAllMyVocabThunk } from '../redux/myVocab/thunk';
// import { AiTwotoneSound } from 'react-icons/ai';
import { Word } from '../redux/myVocab/state';
import React from 'react';
import { MegaphoneFill } from 'react-bootstrap-icons';
// import ReactLoading from 'react-loading';






export default function DictionaryResult(props: { word: Word }) {


    const dispatch = useDispatch();

    let myVocabItems = props.word

    // const loading = () => (   //{ "type": "cubes", "color": "#39A2DB" }
    //     <ReactLoading type={'cubes'} color={'#39A2DB'} height={'20%'} width={'20%'} />
    // );



    useEffect(() => {
        dispatch(loadAllMyVocabThunk())

    }, [])


    return (



        <div>
            {/* {!myVocabItems && loading()} */}
            <CardBody >
                <CardText>
                    {/* phonetics layer */}
                    <div>
                        {myVocabItems.phonetics && myVocabItems.phonetics.length > 0 ? myVocabItems.phonetics!.map((phonetic: any) =>
                            <div>
                                <div className="plus-circle" onClick={() => {
                                    let audio = new Audio(phonetic.audio)
                                    audio.play()
                                }}> {phonetic.text}
                                    <MegaphoneFill className="audioBTN"/>
                                </div>

                                {/* <b>{phonetic.audio}</b> */}

                            </div>

                        )
                            : ""
                        }
                    </div>


                </CardText>


                {/* meanings layer */}
                {myVocabItems.meanings && myVocabItems.meanings.length > 0 ? myVocabItems.meanings.map((meaning: any) => {
                    let partOfSpeechSplit = meaning.partOfSpeech.split(/\s/).reduce((response: any, word: any) => response += word.slice(0, 1), '')
                    let partOfSpeechUI = partOfSpeechSplit.split("").reverse().join('.');

                    return (
                        <div>
                            <p>
                                <Card>
                                    <CardText className="vocab-insideCard">
                                        <div className="partOfSpeechUI">
                                            {partOfSpeechUI}
                                            {/* {console.log("meaning: ", meaning)} */}
                                        </div>

                                    </CardText>

                                    <CardText>

                                        {meaning.definitions && meaning.definitions.length > 0 ? meaning.definitions.map((definition: any) => {
                                            return (
                                                <div className="meaning-insideCard">
                                                    <b><i>definition:</i></b>
                                                    <p className="vocab-inner">{definition.definition}</p>
                                                    {/* {console.log("definition: ", definition.definition)} */}


                                                    {definition.synonyms && definition.synonyms.length > 0 ?
                                                        // definition.synonyms.map((synonym: any) => {
                                                        //     let firstSynonym = synonym.from(synonym)[0]

                                                        // return (
                                                        <div>
                                                            <b><i>synonym:</i></b>
                                                            <p className="vocab-inner">{" \" " + definition.synonyms.join(" \", \"") + " \" "}</p>
                                                            {/* {console.log("synonym: ", synonym)} */}
                                                        </div>
                                                        // )
                                                        // })
                                                        :
                                                        ""}

                                                    {definition.example && definition.example.length > 0 ?
                                                        <div>
                                                            <b><i>example:</i></b>
                                                            <p className="vocab-inner">{definition.example}</p>
                                                            {/* {console.log("synonym: ", synonym)} */}
                                                        </div>
                                                        :
                                                        ""}

                                                </div>

                                            )



                                        }) :
                                            ""}


                                    </CardText>
                                </Card>
                            </p>
                        </div>
                    )

                }) :

                    ""}

                {/* Hi */}

            </CardBody>
        </div>

    )

}