import react, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router'
import { ImQrcode } from 'react-icons/im';
import { IoIosLogOut } from 'react-icons/io';
import { BsPeopleCircle } from 'react-icons/bs';
import Swal from 'sweetalert2'
import Logo from './CUPCAKE.gif';
import {
  Button,
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import { IRootState } from '../redux/store'
import { logoutThunk } from '../redux/auth/thunk'


export const Navigation = (props: any) => {

  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const authorized = useSelector((state: IRootState) => state.auth.token)
  const info = useSelector((state:IRootState) => state.auth.user)
  const [isChange, setIsChange] =useState();





  

  

  return (
    <div>

      <Navbar color="light" light expand="md">
        <div style={{width:"100%",padding:'0 5%',display:'flex', margin:'10 0rem'}}>
          <NavLink href="/">
            <img src={Logo} style={{ width: "80px", height: "80px" }} />
          </NavLink>
          <NavbarToggler onClick={toggle} style={{marginRight:'20rem', width:'5rem', marginLeft:'1rem', marginTop:'1rem'}} />
          <Collapse isOpen={isOpen} navbar >
            <Nav className="ml-auto" navbar >


              {authorized &&
                <div style={{display:"flex", alignItems:"center"}}>

                  {/* <NavItem>
                    <NavLink href="/image-detection" style={{ fontSize: "20px", marginLeft: "2rem", color: "#6372ff", fontWeight:"bold"}}>

                      Image detection

                    </NavLink>
                  </NavItem> */}


                  <NavItem>
                    <NavLink href="/notebook" style={{ fontSize: "20px", marginLeft: "2rem", color: "#6372ff", fontWeight:"bold" }}>

                      Notebook

                    </NavLink>
                  </NavItem>


                  <NavItem>
                    <NavLink href="/withqrcode" style={{ fontSize: "23px", marginLeft: "2rem", color: "#6372ff"}}>

                      <ImQrcode />

                    </NavLink>
                  </NavItem>


                  <NavItem>
                    <NavLink onClick={() => dispatch(logoutThunk())} style={{ fontSize: "23px", marginLeft: "2rem", color: "#6372ff"}}>

                      <IoIosLogOut />

                    </NavLink>
                  </NavItem>

                  <NavItem>
                    <NavLink  style={{ fontSize: "23px", marginLeft: "2rem", color: "#6372ff"}}>

                      <BsPeopleCircle /> hi,{info?.username}

                    </NavLink>
                  </NavItem>

                </div>}








              {!authorized && <NavItem>
                <NavLink href="/register" style={{ fontSize: "3rem", fontWeight: "bold",color: "#000" }}>Register</NavLink>
              </NavItem>}

              {!authorized && <NavItem>
                <NavLink href="/login" style={{ fontSize: "3rem", fontWeight: "bold", marginLeft: "2rem",color: "#000"  }}>Login</NavLink>
              </NavItem>}

              {!authorized && <NavItem>
                <NavLink href="/scancode" style={{ fontSize: "3rem", fontWeight: "bold", marginLeft: "2rem", color: "#FF616D" }}> <ImQrcode /> </NavLink>
              </NavItem>}

              {/* {!authorized && <NavItem>
                <NavLink href="/mobileCam" style={{ fontSize: "20px", fontWeight: "bold", marginLeft: "2rem" }}>mobileCam</NavLink>
                </NavItem>}
               */}

            </Nav>
          </Collapse>
        </div>
      </Navbar>

    </div>
  );
}


