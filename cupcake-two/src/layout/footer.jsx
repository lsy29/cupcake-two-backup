import React from 'react';
import {Col } from 'reactstrap';
import './footer.css'

export const Contact = (props) => {
  return (
    <div className="main-container">
      <div id='contact'>
        <div style={{ width: "90%", height:"15rem", display:"flex", justifyContent:'center'}}>
        <Col sm="12" md={{ size: 6, offset: 3 }}>
          <div className='col-md-12'>
     
            <div className='row'>
              <div className='social'>
                <ul>
                  <li>
                    <a href={props.data ? props.data.facebook : '/'}>
                      <i className='fa fa-facebook'></i>
                    </a>
                  </li>
                  <li>
                    <a href={props.data ? props.data.twitter : '/'}>
                      <i className='fa fa-twitter'></i>
                    </a>
                  </li>
                  <li>
                    <a href={props.data ? props.data.youtube : '/'}>
                      <i className='fa fa-instagram'></i>
                    </a>
                  </li>
                </ul>
                <p style={{ fontSize: "2.5rem"}}>
                We build CUPCAKE to help people learn english in a fun and easy way.
                </p>
              </div>
            </div>
          
          </div>
        </Col>
        </div>
      </div>

    </div>
  )
}

