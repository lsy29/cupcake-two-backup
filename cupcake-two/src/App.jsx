import { useState, useEffect } from 'react'
import { Navigation } from './layout/navigation'
import { Header } from './components/main'
import { Features } from './components/features'
// import { Channel } from './components/channel'
import { Contact } from './layout/footer'
import JsonData from './data/data.json'
import SmoothScroll from 'smooth-scroll'
import SignInPage from './pages/SignInPage';
import RegisterPage from './pages/RegisterPage';
import NotebookPage from './pages/NotebookPage';
import {Route, Switch } from 'react-router'
import VideoPage from './pages/VideoPage'
import VideoPageTesting from './pages/VideoPageTesting'

import VideoSubtitlePage from './pages/VideoSubtitlePage'
import Img2wordPage from './pages/Img2wordPage'
import ImageDetection from './pages/Img2wordPageTesting'

import ExercisePage from './pages/ExercisePage'
import CheckAnsPage from './pages/CheckAnsPage'
import ScanCodePage from './pages/ScanCodePage'
import WithQRcodePage from './pages/WithQRcodePage'
import { ChannelTesting } from './components/channel-testing'
import UserProfile from './pages/UserProfile'
import QRcodeDemoLoginedPage from './pages/QRcodeDemoLoginedPage'
import QRcodeDemoLoginPage from './pages/QRcodeDemoLoginPage'
// import QRcodeTest from './pages/QRcodeTest'


export const scroll = new SmoothScroll('a[href*="#"]', {
  speed: 1000,
  speedAsDuration: true,
})

const App = () => {
  const [landingPageData, setLandingPageData] = useState({})
  useEffect(() => {
    setLandingPageData(JsonData)
  }, [])

  return (
    <div>

      <Switch>

        <Route path="/" exact>
          <Navigation />
          <Header data={landingPageData.Header} />
          {/* <Channel /> */}
          <ChannelTesting/>
          <Features data={landingPageData.Features} />
          <Contact data={landingPageData.Contact} />
        </Route>

        <Route path="/register"><RegisterPage /></Route>
        <Route path="/login"><SignInPage /></Route>
        <Route path="/video"><VideoPage/></Route>
        <Route path="/videos"><VideoPageTesting/></Route>
        <Route path="/video-subtitle"><VideoSubtitlePage/></Route>
        <Route path="/exercise"><ExercisePage/></Route>
        {/* <Route path="/check-answer"><CheckAnsPage/></Route> */}
        <Route path="/check-answer"><CheckAnsPage/></Route>
        <Route path="/notebook"><NotebookPage/></Route>
        <Route path="/userProfile-dummy"><UserProfile/></Route>
        {/* <Route path="/profile"><PersonalProfilePage/></Route> */}
        <Route path="/qrCode-demo-logined"><QRcodeDemoLoginedPage/></Route>
        <Route path="/qrCode-demo-toBeLogin"><QRcodeDemoLoginPage/></Route>

        {/* <Route path="/userProfile-dummy"><UserProfile/></Route> */}
        <Route path="/scancode"><ScanCodePage/></Route>
        <Route path="/withqrcode"><WithQRcodePage/></Route>

       
        {/* <Route path="/qrCode-demo-logined"><QRcodeDemoLoginedPage/></Route>
        <Route path="/qrCode-demo-toBeLogin"><QRcodeDemoLoginPage/></Route> */}
        {/* <Route path="/mobileCam"><DemoOneOffScanning/></Route> */}
        {/* <Route path="/mobileCamTwo"><DemoContinuousScanning/></Route> */}
        

        <Route path="/img2word"><Img2wordPage/></Route>
        <Route path="/image-detection"><ImageDetection/></Route>

      </Switch>
      
      
    </div>
  )
}

export default App
