import { ExerciseAction } from './action';

export interface ExerciseState {
    Q1: string,
    Q2: string,
    Q3: string,
    Q4: string,
    Q5: string,
}
const initialState: ExerciseState = {
    Q1: '',
    Q2: '',
    Q3: '',
    Q4: '',
    Q5: '',
}

export const exerciseReducer = (state: ExerciseState = initialState, action: ExerciseAction): ExerciseState => {
    switch (action.type) {
        case '@@exercise/SUBMIT_FORM_SUCCESS':
            return {
                Q1: '',
                Q2: '',
                Q3: '',
                Q4: '',
                Q5: '',
            }
        default:
            return state;
    }
}