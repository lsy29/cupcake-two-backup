export function submitFormSuccess(answer: string) {

    return {
        type: '@@exercise/SUBMIT_FORM_SUCCESS' as '@@exercise/SUBMIT_FORM_SUCCESS',
        // token,
        answer
    }
}

export type ExerciseAction = ReturnType<typeof submitFormSuccess>