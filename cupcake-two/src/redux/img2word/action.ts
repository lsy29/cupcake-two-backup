import { i2w } from "./img2wordType"

export function deletedMemos(id: number) {
  return {
    type: '@@Memo/deleted' as const,
    id,
  }
}

export function loadedMemos(memos: i2w) {
  return {
    type: '@@Memo/loaded' as const,
    memos,
  }
}


export function postedImg(imgpath : string, dtwords:Array<string>) {

 
  return {
    type: '@@i2w/preview-ready' as const,
    imgpath,
    dtwords
  }
}

export function pyImg(pyimgpath : string,dtwords:Array<string>) {

 
  return {
    type: '@@i2w/py-ready' as const,
    pyimgpath,
    dtwords
  }
}




export type Img2wordAction =
  | ReturnType<typeof deletedMemos>
  | ReturnType<typeof postedImg>
  | ReturnType<typeof pyImg>

