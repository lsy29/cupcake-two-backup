import { Img2wordState } from './state'
import { initialState } from './state'
import { Img2wordAction } from './action'
import produce, { enableMapSet } from 'immer'
import 'map.prototype.tojson'
enableMapSet()

// export let Img2wordReducer = produce(
//   (state: Img2wordState, action: Img2wordAction): void => {
//     switch (action.type) {

//       case '@@Memo/deleted':
//         state.status = 'posting'
//         action.id=4
//         return

//       case '@@i2w/preview-ready':
        
//         // state.status = 'posted'
//         // state.error = undefined
//         // state.i2wObject=action.imgpath
        
 
          
      

//     }
//   },
//   initialState,
// )

export let Img2wordReducer  = (
  state: Img2wordState = initialState, 
  action: Img2wordAction,
  ) : Img2wordState => {
  switch (action.type) {
      case '@@i2w/py-ready' :
          return {
            ...state,
            status : 'detected',
            error : undefined,

            i2wObject : {
              id : 0 ,
              title : "",
              image  : action.pyimgpath,
              py_image  : "https://keras.io/img/logo-small.png",
              dtwords  : action.dtwords
            }

          }
      case '@@i2w/preview-ready' : 
          return {
            ...state,
            status : 'posted',
            error : undefined,

            i2wObject : {
              id : 0 ,
              title : "",
              image  : action.imgpath,
              py_image  : "https://keras.io/img/logo-small.png",
              dtwords   : action.dtwords

            }

          }
          
     
    

      default: 
          return state;
  }
}
