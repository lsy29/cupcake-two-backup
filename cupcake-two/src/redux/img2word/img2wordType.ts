export declare type i2w = {
    id : number;
    title : string;
    image : string;
    py_image : string;
    dtwords : Array<string>;

};

// export declare type i2w = Array<{
//     id : number;
//     title : string;
//     image : string;
//     py_image : string;
//     dw1 : string;
//     dw2 : string;
//     dw3 : string;
//     dw4 : string;
//     dw5 : string;
// }>;