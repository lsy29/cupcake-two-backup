import { api , REACT_APP_API_SERVER  } from '../../helpers/api'
import { loadedMemos,  postedImg , pyImg, deletedMemos as deletedMemo } from './action'
import { IRootState, IRootThunkDispatch } from '../store'
import Swal from 'sweetalert2'
import { I2W } from './state'
import { searchVocabThunk } from '../notebook/thunk'
import { useSelector } from 'react-redux'




export function sayHelloThunk() {
  
  console.log("Say Hello to EXPRESS !!! ")

  return async (dispatch: IRootThunkDispatch) => {

    let res = await api.get('/reactgreet')

    let message = await res.text()
    console.log(message)
      
      
    
  }
}

export function postImgThunk(form : HTMLFormElement) {

  let formData = new FormData(form)
  console.log(formData)

  return async (dispatch: IRootThunkDispatch) => {
    try {
      console.log("posting image !!! ")
      let res = await api.post('/postimg', formData)
      
      let imgname = await res.text()
      let imgpath = ""+ REACT_APP_API_SERVER + "/" + imgname
      // let imgpath =  imgname
      console.log(imgpath)
      dispatch(postedImg(imgpath,[]))
      // let memo: Memo = await res.json()
      // form.reset()
      // dispatch(loadedMemos([memo]))

    } catch (error) {
      Swal.showValidationMessage(error.message)
    }
  }

}

export function hiPyThunk() {   
  console.log("Say Hello to Python !!! ")
  return async (dispatch: IRootThunkDispatch) => {
    let res = await api.get('/callpython')
    let message = await res.text()
    console.log("python said : " + message)
  }
}



export function dtImgObjThunk(form : HTMLFormElement) {   

  console.log("detecting img")

  let formData = new FormData(form)
  console.log(formData)

  return async (dispatch: IRootThunkDispatch) => {
    try {
      // setTimeout(()=>{
      //   console.log('inside timeout')
      //   dispatch(postedImg('',['']))
      // }
      // ,2000)
      let res = await api.post('/dtimgobj', formData)
      let pyres = await res.json()
      let pyimgpath = ""+ REACT_APP_API_SERVER + "/" + pyres.pyimg_name
      let dtwords=pyres.detected_words
      console.log("dtword ::::",dtwords.length)
      dispatch(postedImg(pyimgpath,dtwords))
      // let memo: Memo = await res.json()
      // form.reset()
      // dispatch(loadedMemos([memo]))

    } catch (error) {
      Swal.showValidationMessage(error.message)
    }
  }  
}

export function imgDictThunk(form : HTMLFormElement) {   

  console.log("imgDictThunk executed")

  console.log("img2dict : clicked .")

  let formData = new FormData(form)
  console.log(formData)

  return async (dispatch: IRootThunkDispatch) => {
    try {
      
      const resultWord = useSelector((state: IRootState) => state.img2word.i2wObject?.dtwords)
      const selectedWord = resultWord![0]

      dispatch(searchVocabThunk(selectedWord))

      let res = await api.post('/img2dict', formData)


    } catch (error) {
      Swal.showValidationMessage(error.message)
    }
  }  
}




