import { i2w } from "./img2wordType"
import waitpic from "../../../src/layout/CUPCAKE.gif"
// import klogo from '../components/img2word/klogo.png';
import dummypic from "../../../src/layout/dummy_pic.jpg"
export type Img2wordState = {
  status: 'wait' | 'posting' | 'posted' | 'detecting' | 'detected'
  error?: string |  null
  i2wObject:i2w | null
}

export const initialState: Img2wordState = {
  status: 'wait',
  error: null,

  i2wObject: {
    id : 0 ,
    title : "",
    image  : dummypic,
    // image  : {klogo},
    py_image  : waitpic,
    dtwords  : [""],

  }
}

export type I2W = i2w
