import { getSubtitleSuccessAction, getSubtitleFailAction } from "./action";
import { IRootThunkDispatch } from "../store";
import { history } from "../store";

const {REACT_APP_API_SERVER} = process.env

export function getSubtitle(videoId: number) {
    return async (dispatch: IRootThunkDispatch) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/splitText/` + videoId)
        const result = await res.json();

        if(res.status!==200){
            dispatch(getSubtitleFailAction(result.msg));
        }else{
            dispatch(getSubtitleSuccessAction(result.videoId, result.content)); // get subtitle from database and split text through the function
        }        
    }

}

