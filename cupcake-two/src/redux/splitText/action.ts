
// export type SplitTextResultType = {
//    content: string | null, 
// }

export function getSubtitleSuccessAction(videoId: number, content: string) {
    return{
        type: "@@SplitText/getSubtitleSuccess",
        videoId,
        content,
    }
}

export function getSubtitleFailAction(msg: string) {
    return {
        type: '@@SplitText/getSubtitleFail' as const,
        msg,
    }
}

// export function SplitTextFailAction(msg: string) {
//     return {
//         type: '@@SplitText/failed' as const,
//         msg,
//     }
// }

export type SplitTextAction = 
 | ReturnType<typeof getSubtitleSuccessAction>
 | ReturnType<typeof getSubtitleFailAction>
//  | ReturnType<typeof SplitTextFailAction>


// export function getSplitTextByThunk (body: {

// })