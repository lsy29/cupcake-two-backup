import { SplitTextAction } from './action';
import { GetSubtitleState, initialState } from './state';


export let SplitTestReducer = (
    state: GetSubtitleState = initialState,
    action: SplitTextAction,
) : GetSubtitleState => {
    switch (action.type) {
        case '@@SplitText/getSubtitleSuccess' : 
            return {
                ...state,
                videoId: action.videoId,
                content: action.content
            }

        case '@@SplitText/getSubtitleFail' : {
        console.log("action = ", action)
            return {
                ...state,
                msg: 'dummy',
                content: null,
            }
        }
        default: 
            return state;
    }
}