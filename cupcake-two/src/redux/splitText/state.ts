export type GetSubtitleState = {
    content: string | null
    msg: string | null
    videoId: number | null
}

export const initialState: GetSubtitleState = {
    content: null,
    msg: null,
    videoId: null
}