import { type } from "os"

export type MyVocabState = {
    token: string | null,
    words: Word [] | null,
    error: string | null,
    vocabId: number | null,
}


export type Word = {
    word?: string ,
    id?: number ,
    phonetics?: [] ,
    meanings?: [] ,
    audio?: string ,
}

export const initialState: MyVocabState = {
    token: localStorage.getItem('token'),
    words: null,
    error: null,
    vocabId: null,
}