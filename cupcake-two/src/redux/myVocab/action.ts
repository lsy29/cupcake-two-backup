
export interface Word {
    word?: string,
    id?: number,
    phonetics?: [],
    meanings?: [],
    audio?: string,
}


// export function getAllMyVocabSuccess (word: string, vocabId: number, phonetics: string, meanings: string, token: string) {
//     return {
//         type: '@@MyVocab/getAllMyVocabSuccess' as const,
//         vocabId,
//         word,
//         phonetics,
//         meanings,
//         token,
//     }
// }



export function getAllMyVocabSuccess (words: Word []) {
    return {
        type: '@@MyVocab/getAllMyVocabSuccess' as const,
        words,
    }
}

export function  getAllMyVocabFail (msg: string) {
    return {
        type: '@@MyVocab/getAllMyVocabFail' as const,
        msg,
    }
}

export function searchVocabSuccess (words: Word []) {
    return {
        type: '@@MyVocab/searchVocabSuccess' as const,
        words,
    }
}

export function searchVocabFail (msg: string) {
    return {
        type: '@@notebook/searchVocabFail' as const,
        msg,
    }

}

export function deleteVocabSuccess (id: number) {
    return {
        type: '@@MyVocab/deleteVocabSuccess' as const,
        id
    }
}

export function deleteVocabFail (msg: string) {
    return {
        type: '@@MyVocab/deleteVocabFail' as const,
        msg
    }
}

export type MyVocabAction = 
 | ReturnType<typeof getAllMyVocabSuccess>
 | ReturnType<typeof getAllMyVocabFail>
 | ReturnType<typeof deleteVocabSuccess>
 | ReturnType<typeof deleteVocabFail>
 | ReturnType<typeof searchVocabSuccess>
 | ReturnType<typeof searchVocabFail>