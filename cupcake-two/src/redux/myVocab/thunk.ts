

import { IRootThunkDispatch } from "../store";
// import { history }from '../store'
import { deleteVocabFail, deleteVocabSuccess, getAllMyVocabFail, getAllMyVocabSuccess , searchVocabSuccess, searchVocabFail} from "./action";

const {REACT_APP_API_SERVER} = process.env

export function loadAllMyVocabThunk() {
    return async(dispatch:IRootThunkDispatch)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/getMyVocab` , {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charsct=utf-8',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }

        })

        let result:any = await res.json()

        if(res.status!==200){
            dispatch(getAllMyVocabFail(result.msg));
        }else{
            dispatch(getAllMyVocabSuccess(result))
            console.log("word from thunk: ", result);
            
        }

    }
}

export function searchMyVocabThunk(seachWord:string) {
    return async (dispatch: any) => {
        //edgar
        const res = await fetch(`${REACT_APP_API_SERVER}/search/${seachWord}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charsct=utf-8',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })

        let result: any = await res.json()
        console.log(result)

        if (res.ok) {
            dispatch(searchVocabSuccess(result))
        }else{
            dispatch(searchVocabFail('load notebook fail'))
        }


    }
}

export function deleteVocabThunk(id: any) {
    return async(dispatch:IRootThunkDispatch) => {
        try{

        
            const res = await fetch(`${REACT_APP_API_SERVER}/deleteMyVocab/` + id, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json; charsct=utf-8',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            const result =  await res.text();

            if (res.status !== 202){
                dispatch(deleteVocabFail(result))
            }else {
                dispatch(deleteVocabSuccess(id));
                loadAllMyVocabThunk() 
             }

        } catch (error) {
            console.error(error)
            dispatch(deleteVocabFail('system fail'))
        }

    }

}