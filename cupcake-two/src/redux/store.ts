import { CallHistoryMethodAction, connectRouter, routerMiddleware, RouterState } from "connected-react-router";
import { createBrowserHistory } from "history";
import { combineReducers, createStore, compose, applyMiddleware } from "redux";
import { AuthAction } from './auth/action';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { authReducer, AuthState } from "./auth/reducer";
import { RegisterAction } from "./register/action";
import { NotebookState, notebookReducer } from "./notebook/reducer";
import { NotebookAction } from "./notebook/action";
// inserted for img2word - start //
import { Img2wordState } from './img2word/state'
import { Img2wordAction } from './img2word/action'
import { Img2wordReducer } from './img2word/reducer'
import { FBAuthAction } from "./fbAuth/action";
import { SplitTextAction } from "./splitText/action";
// import { SplitTextState } from "./splitText/state";

import { VideoState, videoReducer} from "./video/reducer"
import { MyVocabAction } from "./myVocab/action";
import { MyVocabState } from "./myVocab/state";
import { MyVocabReducer } from "./myVocab/reducer";
import { exerciseReducer, ExerciseState } from "./exercise/reducer";
import { ExerciseAction } from "./exercise/action";
// import { subtitleReducer, VideoSubtitleState } from "./subtitle/reducer";


export const history = createBrowserHistory();

export interface RootState {
    router: RouterState
    auth: AuthState
}

// inserted for img2word - end //

export interface IRootState {
    router: RouterState,
    auth: AuthState,
    notebook:NotebookState
    img2word: Img2wordState,
      //video
    exercise: ExerciseState
    video: VideoState,
    myVocab: MyVocabState,
    // subtitle: VideoSubtitleState

}

const rootReducer = combineReducers<IRootState>({
    router: connectRouter(history),
    auth: authReducer,
    notebook: notebookReducer,
    img2word: Img2wordReducer,
    myVocab: MyVocabReducer,
    exercise: exerciseReducer,
      //video
    video: videoReducer,
    // subtitle: subtitleReducer
  })


type IRootAction = RegisterAction | AuthAction | NotebookAction| FBAuthAction | Img2wordAction | SplitTextAction | MyVocabAction | ExerciseAction


export type IRootThunkDispatch = ThunkDispatch<IRootState,null,IRootAction>


declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose


const rootEnhancer = composeEnhancers (
  applyMiddleware(thunk),
  applyMiddleware(routerMiddleware(history)),
)


export const store = createStore<IRootState, IRootAction, {}, {}>(
  rootReducer,
  rootEnhancer
)




// export const store = createStore(rootReducer, composeEnhancers(
//     applyMiddleware(routerMiddleware(history)),
//     applyMiddleware(thunk)
// ))
