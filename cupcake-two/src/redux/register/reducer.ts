import {RegisterAction} from './action'


export interface RegisterState {
    isRegistered: boolean
    username: string | null
    password: number | null
}


const initialState: RegisterState = {
    isRegistered: false,
    username: null,
    password: null
}

export const registerReducer = (state:RegisterState = initialState, action: RegisterAction): RegisterState => {
    if (action.type === '@@register/REGISTER_SUCCESS') {
        return {
            ...state, 
            isRegistered: true
        }
    }

    if (action.type === '@@register/REGISTER_FAILED') {
        return {
            ...state, 
            isRegistered: false,
            username: 'wrong username',
            password: null
            
        }
    }

  
    return state;  
}