import Swal from 'sweetalert2'
import { push } from "connected-react-router"
import { history} from '../store'

const {REACT_APP_API_SERVER} = process.env


export function registerThunk (registerForm:any ) {
    return async (dispatch:any)=> {
        //edgar
        const res = await fetch (`${REACT_APP_API_SERVER}/register`,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charsct=utf-8'
                },
                body: JSON.stringify(registerForm),
               
        })

        console.log(res)

        if (res.status === 201){
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: ' Registered and you can login now!',
                showConfirmButton: true,
                
            })
            history.push('/')    
        }
        

         if (res.status === 400){
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: ' Missing username or password!',
                showConfirmButton: false,
                timer: 2000
            })
               
        }
 
    } 
} 