export function RegisterSuccess (username: string, password: number) {
    return {
        type: '@@register/REGISTER_SUCCESS' as '@@register/REGISTER_SUCCESS',
        username,
        password
    }
}


export function RegisterFailed (username: string, password: number) {
    return {
        type: '@@register/REGISTER_FAILED' as '@@register/REGISTER_FAILED',
        username,
        password
    }
}



export type RegisterAction = ReturnType<typeof RegisterSuccess> |
                         ReturnType<typeof RegisterFailed> 
                         