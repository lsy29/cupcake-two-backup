import { Word } from "./reducer"

export function loadNoteBook (vocabs: Word) {
    return {
        type: '@@notebook/LOAD_WORD' as '@@notebook/LOAD_WORD',
        vocabs
    }
}
export function loadNoteBookFail (msg: string) {
    return {
        type: '@@notebook/LOAD_WORD/failed' as const,
        msg,
    }

}


export function addMyVocab (user_id: number, vocabs: string) {
    return {
        type: '@@notebook/ADD_MY_VOCAB' as '@@notebook/ADD_MY_VOCAB',
        user_id,
        vocabs,
       
    }
}


export function deleteMyVocab (user_id: number, dictionary_id: number) {
    return {
        type: '@@notebook/DELETE_MY_VOCAB' as '@@notebook/DELETE_MY_VOCAB',
        
    }
}




export type NotebookAction = ReturnType<typeof addMyVocab> |
                             ReturnType<typeof deleteMyVocab>|
                             ReturnType<typeof loadNoteBook> |
                             ReturnType<typeof loadNoteBookFail> 
                             





