// import { Dispatch } from "react"
// import { loadSubtitle } from "./action"
 

export interface VideoSubtitle {
    youtube_video_id: any,
    video_title: string,
    subtitle: string 
}

export interface VideoSubtitleState{
    selectedVideoSubtitle: string
}

const initialState: VideoSubtitle = {
    youtube_video_id: " ",
    video_title: " ",
    subtitle: " "
}


// export const subtitleReducer = (state: VideoSubtitleState = initialState, action: any): VideoSubtitleState => {
//     if (action.type === '@@LOAD_SUBTITLE'){
//         return state
//     }
//     return state
// }