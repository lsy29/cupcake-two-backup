export function loginSuccess(token: string) {

    return {
        type: '@@auth/LOGIN_SUCCESS' as '@@auth/LOGIN_SUCCESS',
        token,
    }
}


export function loginFailed(msg: string) {
    return {
        type: '@@auth/LOGIN_FAILED' as '@@auth/LOGIN_FAILED',
        msg,
    }
}


export function logoutSuccess() {
    return {
        type: '@@auth/LOGOUT_SUCCESS' as '@@auth/LOGOUT_SUCCESS'
    }
}

export type AuthAction = ReturnType<typeof loginSuccess> |
    ReturnType<typeof loginFailed> |
    ReturnType<typeof logoutSuccess>


// export function login(username: string, password: string) {
//     return async (dispatch: Dispatch<any>) => {
//         const res = await fetch('http://locahost:8100/login', {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify({ username, password })
//         })
//         const json = await res.json();

//         if (json.username) {
//             dispatch(loginSuccess(json.username))
//         } else {
//             dispatch(loginFailed())
//         }
//     }
// }



