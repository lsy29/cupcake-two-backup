export type AuthState = {
    token: string | null
    user : AuthUser | null
    error: string | null
}

export type AuthUser = {
    id: number,
    email: string,
    icon: string,
    name: string,
    
}

export const initialState: AuthState = {  // cocept about local storage
    token: localStorage.getItem('token'), 
    user: null,
    error: null,
}  // if had been login before it will have the previous token

// no user at the initial state
