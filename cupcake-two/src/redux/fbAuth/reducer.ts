import { FBAuthAction } from './action';
import { AuthState, AuthUser, initialState } from './state';
import decodeJWT from 'jwt-decode';

export let AuthReducer = (
    state: AuthState = initialState, 
    action: FBAuthAction,
    ) : AuthState => {
    switch (action.type) {
        case '@@Auth/logout' : 
            return {
                token: null, 
                user: null, 
                error: null,
            }
       
        case '@@Auth/failed' :
            return {
                error: action.msg,
                user: null,
                token: null,
            }


        case '@@Auth/load_token' :
            try {
                const user: AuthUser = decodeJWT(action.token)
                return {
                    error: null,
                    user,
                    token: action.token,
                }  
            } catch (error) {
                return{
                    error: 'invaild Token',
                    user: null,
                    token: null
                }
            }


        default: 
            return state;
    }
}